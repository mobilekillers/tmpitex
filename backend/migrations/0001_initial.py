# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cable',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('attenuation', models.FloatField(null=True, verbose_name='\u0417\u0430\u0442\u0443\u0445\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': '\u041a\u0430\u0431\u0435\u043b\u044c',
                'verbose_name_plural': '\u041a\u0430\u0431\u0435\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='CableParameter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
                ('cable', models.ForeignKey(related_name='parameters', verbose_name='\u041a\u0430\u0431\u0435\u043b\u044c', to='backend.Cable')),
                ('type', models.ForeignKey(verbose_name='\u0422\u0438\u043f', to='dictionaries.CableParameterType')),
            ],
            options={
                'verbose_name': '\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440 \u043a\u0430\u0431\u0435\u043b\u044f',
                'verbose_name_plural': '\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b \u043a\u0430\u0431\u0435\u043b\u044f',
            },
        ),
        migrations.CreateModel(
            name='Connection',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
            ],
            options={
                'verbose_name': '\u0421\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0421\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.CreateModel(
            name='ConnectionPoint',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('name', models.CharField(max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('type', models.CharField(max_length=200, null=True, verbose_name='\u0422\u0438\u043f', blank=True)),
            ],
            options={
                'verbose_name': '\u0422\u043e\u0447\u043a\u0430 \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u0422\u043e\u0447\u043a\u0438 \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.CreateModel(
            name='ConnectionUnit',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Fiber',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('number', models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432\u043d\u0443\u0442\u0440\u0438 \u043c\u043e\u0434\u0443\u043b\u044f')),
                ('color', models.ForeignKey(verbose_name='\u0426\u0432\u0435\u0442', blank=True, to='dictionaries.Color', null=True)),
            ],
            options={
                'ordering': ['number'],
                'verbose_name': '\u0412\u043e\u043b\u043e\u043a\u043d\u043e',
                'verbose_name_plural': '\u0412\u043e\u043b\u043e\u043a\u043d\u0430',
            },
        ),
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('number', models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432\u043d\u0443\u0442\u0440\u0438 \u043a\u0430\u0431\u0435\u043b\u044f')),
                ('cable', models.ForeignKey(related_name='modules', verbose_name='\u0422\u0438\u043f \u043a\u0430\u0431\u0435\u043b\u044f', to='backend.Cable')),
                ('color', models.ForeignKey(verbose_name='\u0426\u0432\u0435\u0442', blank=True, to='dictionaries.Color', null=True)),
            ],
            options={
                'verbose_name': '\u041c\u043e\u0434\u0443\u043b\u044c',
                'verbose_name_plural': '\u041c\u043e\u0434\u0443\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='Splitter',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('serial', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0435\u0440\u0438\u0439\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', blank=True)),
                ('connect_point', models.ForeignKey(related_name='splitters', verbose_name='\u0422\u043e\u0447\u043a\u0430 \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f', to='backend.ConnectionPoint')),
            ],
            options={
                'verbose_name': '\u0421\u043f\u043b\u0438\u0442\u0442\u0435\u0440',
                'verbose_name_plural': '\u0421\u043f\u043b\u0438\u0442\u0442\u0435\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='SplitterOutputAttenuation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('output_number', models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432\u044b\u0445\u043e\u0434\u0430')),
                ('attenuation', models.FloatField(verbose_name='\u0417\u0430\u0442\u0443\u0445\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u0442\u0443\u0445\u0430\u043d\u0438\u0435 \u0432\u044b\u0445\u043e\u0434\u0430 \u0441\u043f\u043b\u0438\u0442\u0442\u0435\u0440\u0430',
                'verbose_name_plural': '\u0417\u0430\u0442\u0443\u0445\u0430\u043d\u0438\u0435 \u0432\u044b\u0445\u043e\u0434\u043e\u0432 \u0441\u043f\u043b\u0438\u0442\u0442\u0435\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='SplitterType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('inputs', models.IntegerField(verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0432\u0445\u043e\u0434\u043e\u0432')),
                ('outputs', models.IntegerField(verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0432\u044b\u0445\u043e\u0434\u043e\u0432')),
                ('attenuation', models.FloatField(null=True, verbose_name='\u041e\u0431\u0449\u0435\u0435 \u0437\u0430\u0442\u0443\u0445\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0421\u043f\u0435\u0446\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u044f \u0441\u043f\u043b\u0438\u0442\u0442\u0435\u0440\u0430',
                'verbose_name_plural': '\u0421\u043f\u0435\u0446\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438 \u0441\u043f\u043b\u0438\u0442\u0442\u0435\u0440\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='SupportPoint',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('title', models.CharField(max_length=255, blank=True)),
                ('latitude', models.FloatField(verbose_name='\u0428\u0438\u0440\u043e\u0442\u0430')),
                ('longitude', models.FloatField(verbose_name='\u0414\u043e\u043b\u0433\u043e\u0442\u0430')),
                ('comment', models.TextField(null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('layer', models.CharField(default=b'fact', max_length=4, verbose_name='\u0421\u043b\u043e\u0439', choices=[(b'plan', '\u041f\u043b\u0430\u043d'), (b'fact', '\u0424\u0430\u043a\u0442'), (b'all', '\u0412\u0441\u0435')])),
                ('type', models.ForeignKey(verbose_name='\u0422\u0438\u043f', blank=True, to='dictionaries.SupportPointType', null=True)),
            ],
            options={
                'verbose_name': '\u0422\u043e\u0447\u043a\u0430 \u043e\u043f\u043e\u0440\u044b',
                'verbose_name_plural': '\u0422\u043e\u0447\u043a\u0438 \u043e\u043f\u043e\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='UserSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('zoom', models.IntegerField(default=15)),
                ('latitude', models.FloatField(default=55.88)),
                ('longitude', models.FloatField(default=36.87585)),
                ('layer', models.CharField(default=b'fact', max_length=10)),
                ('user', models.OneToOneField(related_name='map_settings', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ConnectionUnitFiber',
            fields=[
                ('connectionunit_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='backend.ConnectionUnit')),
            ],
            options={
                'verbose_name': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442 \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f (\u0432\u043e\u043b\u043e\u043a\u043d\u043e)',
                'verbose_name_plural': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442\u044b \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f (\u0432\u043e\u043b\u043e\u043a\u043d\u043e)',
            },
            bases=('backend.connectionunit',),
        ),
        migrations.CreateModel(
            name='ConnectionUnitSplitter',
            fields=[
                ('connectionunit_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='backend.ConnectionUnit')),
                ('type', models.IntegerField(verbose_name='\u0422\u0438\u043f', choices=[(0, b'\xd0\x92\xd1\x8b\xd1\x85\xd0\xbe\xd0\xb4'), (1, b'\xd0\x92\xd1\x85\xd0\xbe\xd0\xb4')])),
                ('number', models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432\u0445\u043e\u0434\u0430/\u0432\u044b\u0445\u043e\u0434\u0430')),
            ],
            options={
                'verbose_name': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442 \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f (\u0441\u043f\u043b\u0438\u0442\u0442\u0435\u0440)',
                'verbose_name_plural': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442\u044b \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f (\u0441\u043f\u043b\u0438\u0442\u0442\u0435\u0440)',
            },
            bases=('backend.connectionunit',),
        ),
        migrations.CreateModel(
            name='Segment',
            fields=[
                ('connectionunit_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='backend.ConnectionUnit')),
                ('length', models.FloatField(null=True, verbose_name='\u0414\u043b\u0438\u043d\u0430', blank=True)),
                ('attenuation', models.FloatField(null=True, verbose_name='\u0417\u0430\u0442\u0443\u0445\u0430\u043d\u0438\u0435', blank=True)),
                ('cable', models.ForeignKey(verbose_name='\u041a\u0430\u0431\u0435\u043b\u044c', blank=True, to='backend.Cable', null=True)),
                ('end_point', models.ForeignKey(related_name='ending_segmnts', verbose_name='\u0422\u043e\u0447\u043a\u0430 \u043a\u043e\u043d\u0446\u0430', to='backend.SupportPoint')),
                ('start_point', models.ForeignKey(related_name='starting_segments', verbose_name='\u0422\u043e\u0447\u043a\u0430 \u043d\u0430\u0447\u0430\u043b\u0430', to='backend.SupportPoint')),
            ],
            options={
                'verbose_name': '\u0421\u0435\u0433\u043c\u0435\u043d\u0442',
                'verbose_name_plural': '\u0421\u0435\u0433\u043c\u0435\u043d\u0442\u044b',
            },
            bases=('backend.connectionunit',),
        ),
        migrations.AddField(
            model_name='splitteroutputattenuation',
            name='type',
            field=models.ForeignKey(related_name='attenuations', verbose_name='\u0422\u0438\u043f \u0441\u043f\u043b\u0438\u0442\u0442\u0435\u0440\u0430', to='backend.SplitterType'),
        ),
        migrations.AddField(
            model_name='splitter',
            name='type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f', to='backend.SplitterType'),
        ),
        migrations.AddField(
            model_name='fiber',
            name='module',
            field=models.ForeignKey(related_name='fibers', verbose_name='\u041c\u043e\u0434\u0443\u043b\u044c', to='backend.Module'),
        ),
        migrations.AddField(
            model_name='connectionpoint',
            name='support_point',
            field=models.ForeignKey(related_name='connection_points', verbose_name='\u0422\u043e\u0447\u043a\u0430 \u043e\u043f\u043e\u0440\u044b', blank=True, to='backend.SupportPoint', null=True),
        ),
        migrations.AddField(
            model_name='connection',
            name='connect_point',
            field=models.ForeignKey(related_name='connections', verbose_name='\u0422\u043e\u0447\u043a\u0430 \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u044f', blank=True, to='backend.ConnectionPoint', null=True),
        ),
        migrations.AddField(
            model_name='connection',
            name='left_unit',
            field=models.ForeignKey(related_name='left_connections', verbose_name='\u041f\u0435\u0440\u0432\u044b\u0439 \u044d\u043b\u0435\u043c\u0435\u043d\u0442', to='backend.ConnectionUnit'),
        ),
        migrations.AddField(
            model_name='connection',
            name='right_unit',
            field=models.ForeignKey(related_name='right_connections', verbose_name='\u0412\u0442\u043e\u0440\u043e\u0439 \u044d\u043b\u0435\u043c\u0435\u043d\u0442', to='backend.ConnectionUnit'),
        ),
        migrations.AddField(
            model_name='connection',
            name='support_point',
            field=models.ForeignKey(verbose_name='\u0422\u043e\u0447\u043a\u0430 \u043e\u043f\u043e\u0440\u044b', to='backend.SupportPoint'),
        ),
        migrations.AddField(
            model_name='connectionunitsplitter',
            name='splitter',
            field=models.ForeignKey(verbose_name='\u0421\u043f\u043b\u0438\u0442\u0442\u0435\u0440', to='backend.Splitter'),
        ),
        migrations.AddField(
            model_name='connectionunitfiber',
            name='fiber',
            field=models.ForeignKey(verbose_name='\u0412\u043e\u043b\u043e\u043a\u043d\u043e', to='backend.Fiber'),
        ),
        migrations.AddField(
            model_name='connectionunitfiber',
            name='segment',
            field=models.ForeignKey(verbose_name='\u0421\u0435\u0433\u043c\u0435\u043d\u0442', to='backend.Segment'),
        ),
    ]
