# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0004_auto_20151010_2004'),
        ('backend', '0002_auto_20151010_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='segment',
            name='brigade',
            field=models.ForeignKey(verbose_name='\u0411\u0440\u0438\u0433\u0430\u0434\u0430 (\u043a\u0442\u043e \u0441\u0442\u0440\u043e\u0438\u043b)', blank=True, to='dictionaries.Brigade', null=True),
        ),
    ]
