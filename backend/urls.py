from django.conf.urls import include, url
from backend import views

from rest_framework.routers import DefaultRouter

connection_point_router = DefaultRouter()
connection_point_router.register(r'connection_point', views.ConnectionPointViewset)

urlpatterns = (
    url(r'^cables/$', views.CableListView.as_view()),
    url(r'^cables/(?P<pk>[^/]+)/$', views.CableView.as_view()),
    url(r'^routes/$', views.RoutesView.as_view()),
    url(r'^point/move/$', views.MovePointsView.as_view()),
    url(r'^point/layer/$', views.ChangePointsLayerView.as_view()),

    url(r'^point/(?P<pk>[^/]+)/connections/$', views.PointConnectionsView.as_view()),
    url(r'^point/(?P<pk>[^/]+)/$', views.PointView.as_view()),

    url(r'^segment/merge/$', views.MergeSegments.as_view()),
    url(r'^segment/can_merge/(?P<point>[^/]+)/(?P<id1>[^/]+)/(?P<id2>[^/]+)$', views.CanMergeSegments.as_view()),
    url(r'^segment/can_connect/(?P<point>[^/]+)/(?P<id1>[^/]+)/(?P<id2>[^/]+)$', views.CanConnectSegments.as_view()),
    url(r'^segment/split/(?P<point>[^/]+)/(?P<id1>[^/]+)/(?P<id2>[^/]+)$', views.SplitSegments.as_view()),
    url(r'^segment/(?P<pk>[^/]+)/$', views.SegmentView.as_view()),

    url(r'^structure/(?P<id1>[^/]+)/(?P<id2>[^/]+)/$', views.SegmentsStructure.as_view()),
    url(r'^splitter_type/(?P<pk>[^/]+)/$', views.SplitterTypeView.as_view()),
    url(r'^splitter/(?P<pk>[^/]+)/$', views.SplitterView.as_view()),
    url(r'^settings/$', views.UpdateSettingsView.as_view()),
    url(r'^', include(connection_point_router.urls)),
)
