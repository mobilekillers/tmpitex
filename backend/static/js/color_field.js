$(document).ready(function () {
    $(".inline-related").on("click", ".add-row a", function () {
        setTimeout(function () {
            var formrow = $(".form-row:not(.empty-form)").last();
            formrow.hide();
            if (formrow.length) {
                pickerForSelect.bind($('select[id$=color]', formrow))();
                formrow.show();
            }
        }, 10);
    });

    $(".form-row select[id$=color], #id_color").each(function () {
        var container = $(this).closest('div.related-widget-wrapper');
        container.css({'overflow-x': 'visible', 'overflow-y': 'visible'});
        container.find('.related-widget-wrapper-link').remove();
        $(this).hide();
    });

    var selects = $(".form-row:not(.empty-form) select[id$=color], #id_color");
    selects.each(pickerForSelect);

    function pickerForSelect() {
        var colors = $("option", this).map(function () {
            return $(this).html()
        });
        var picker = CreateColorPicker(colors);
        picker.insertAfter(this);
        var $select = $(this);
        var color = $select.find("option[value=" + $select.val() + "]").html();
        picker.dropdown({
            value: color, selector: "color", direction: "down", control: this, callback: function (val) {
                var option = $select.find("option:contains(" + val + ")");
                $select.val(option.val());
            }
        });
    }

    function CreateColorPicker(colors) {
        var ul = $("<ul></ul>");
        for (var i = 0; i < colors.length; i++) {
            var colParts = colors[i].split("#");
            var hasRing = colParts[0] == '1';
            var color = "#" + colParts[1];
            var li = $("<li data-color='" + colors[i] + "'></li>").append("<div class='colPick large' style='background-color:" + color + "'></div>")
                .append(hasRing ? "&nbsp;+кольцо" : "&nbsp;");
            ul.append(li);
        }

        return ul;
    }
});

