# coding: utf-8

import uuid

from django.db import models
from model_utils import Choices
from django.conf import settings
from django.db.models import Q


class Cable(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    title = models.CharField(u'Название', max_length=255)
    description = models.TextField(u'Описание', blank=True, null=True)
    attenuation = models.FloatField(u'Затухание', blank=True, null=True)

    @staticmethod
    def as_list():
        return Cable.objects.all().values('id', 'title')

    class Meta:
        verbose_name = u'Кабель'
        verbose_name_plural = u'Кабели'
        ordering = ['title']

    def __unicode__(self):
        return self.title


class CableParameter(models.Model):
    cable = models.ForeignKey(Cable, related_name='parameters', verbose_name=u'Кабель')
    type = models.ForeignKey('dictionaries.CableParameterType', verbose_name=u'Тип')
    value = models.CharField(u'Значение', max_length=255)

    class Meta:
        verbose_name = u'Параметр кабеля'
        verbose_name_plural = u'Параметры кабеля'

    def __unicode__(self):
        return self.type.name


class ConnectionPoint(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    name = models.CharField(u'Название', max_length=200, blank=True, null=True)
    type = models.CharField(u'Тип', max_length=200, blank=True, null=True)
    support_point = models.ForeignKey('SupportPoint', blank=True, null=True, verbose_name=u'Точка опоры',
                                      related_name='connection_points')

    class Meta:
        verbose_name = u'Точка соединения'
        verbose_name_plural = u'Точки соединения'

    def __unicode__(self):
        return self.name


class ConnectionUnit(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)


class ConnectionUnitFiber(ConnectionUnit):
    segment = models.ForeignKey('Segment', verbose_name=u'Сегмент')
    fiber = models.ForeignKey('Fiber', verbose_name=u'Волокно')

    class Meta:
        verbose_name = u'Элемент соединения (волокно)'
        verbose_name_plural = u'Элементы соединения (волокно)'

    def __unicode__(self):
        return u'Элемент соединения (волокно)'


class ConnectionUnitSplitter(ConnectionUnit):
    TYPE = Choices((0, 'output', 'Выход'), (1, 'input', 'Вход'))
    splitter = models.ForeignKey('Splitter', verbose_name=u'Сплиттер')
    type = models.IntegerField(u'Тип', choices=TYPE)
    number = models.IntegerField(u'Номер входа/выхода')

    class Meta:
        verbose_name = u'Элемент соединения (сплиттер)'
        verbose_name_plural = u'Элементы соединения (сплиттер)'

    def __unicode__(self):
        return u'Элемент соединения (сплиттер)'


# TODO: remove left_unit and right_unit when removed it they are not segments
class Connection(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    support_point = models.ForeignKey('SupportPoint', verbose_name=u'Точка опоры')
    left_unit = models.ForeignKey(ConnectionUnit, verbose_name=u'Первый элемент', related_name='left_connections')
    right_unit = models.ForeignKey(ConnectionUnit, verbose_name=u'Второй элемент', related_name='right_connections')
    connect_point = models.ForeignKey(ConnectionPoint, verbose_name=u'Точка соединения', related_name='connections',
                                      null=True, blank=True)

    class Meta:
        verbose_name = u'Соединение'
        verbose_name_plural = u'Соединения'

    def __unicode__(self):
        return u'Соединение'

    def delete(self, using=None):
        if not self.left_unit is Segment:
            self.left_unit.delete()
        if not self.right_unit is Segment:
            self.right_unit.delete()
        super(Connection, self).delete(using)

class Fiber(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    number = models.IntegerField(u'Номер внутри модуля')
    color = models.ForeignKey('dictionaries.Color', blank=True, null=True, verbose_name=u'Цвет')
    module = models.ForeignKey('Module', verbose_name=u'Модуль', related_name='fibers')

    class Meta:
        verbose_name = u'Волокно'
        verbose_name_plural = u'Волокна'
        ordering = ['number']

    def __unicode__(self):
        return u'Волокно'


class Module(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    number = models.IntegerField(u'Номер внутри кабеля')
    color = models.ForeignKey('dictionaries.Color', blank=True, null=True, verbose_name=u'Цвет')
    cable = models.ForeignKey(Cable, related_name='modules', verbose_name=u'Тип кабеля')

    class Meta:
        verbose_name = u'Модуль'
        verbose_name_plural = u'Модули'
        ordering = ['number']

    def fiber_wise(self):
        return u'Многоволоконный' if self.fibers.count() > 1 else u'Одноволоконный'
    fiber_wise.short_description = u"Волокна"

    def __unicode__(self):
        return u"#%s в %s" % (self.number, self.cable.title)


class Segment(ConnectionUnit):
    start_point = models.ForeignKey('SupportPoint', related_name='starting_segments', verbose_name=u'Точка начала')
    end_point = models.ForeignKey('SupportPoint', related_name='ending_segmnts', verbose_name=u'Точка конца')
    cable = models.ForeignKey(Cable, verbose_name=u'Кабель', blank=True, null=True)
    brigade = models.ForeignKey('dictionaries.Brigade', verbose_name=u'Бригада (кто строил)', blank=True, null=True)
    length = models.FloatField(u'Длина', blank=True, null=True)
    attenuation = models.FloatField(u'Затухание', blank=True, null=True)

    class Meta:
        verbose_name = u'Сегмент'
        verbose_name_plural = u'Сегменты'

    @staticmethod
    def for_point(point_id):
        queryset = Segment.objects.select_related('cable', 'end_point', 'start_point')
        return queryset.filter(Q(end_point_id=point_id) | Q(start_point_id=point_id))

    def __unicode__(self):
        return u'Сегмент'


class Splitter(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    type = models.ForeignKey('SplitterType', verbose_name=u'Тип')
    serial = models.CharField(u'Серийный номер', max_length=255, blank=True, null=True)
    connect_point = models.ForeignKey(ConnectionPoint, related_name='splitters', verbose_name=u'Точка соединения')

    class Meta:
        verbose_name = u'Сплиттер'
        verbose_name_plural = u'Сплиттеры'

    def __unicode__(self):
        return u'Сплиттер'


class SplitterOutputAttenuation(models.Model):
    type = models.ForeignKey('SplitterType', related_name='attenuations', verbose_name=u'Тип сплиттера')
    output_number = models.IntegerField(u'Номер выхода')
    attenuation = models.FloatField(u'Затухание')

    class Meta:
        verbose_name = u'Затухание выхода сплиттера'
        verbose_name_plural = u'Затухание выходов сплиттера'


class SplitterType(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    title = models.CharField(u'Название', max_length=255)
    inputs = models.IntegerField(u'Количество входов')
    outputs = models.IntegerField(u'Количество выходов')
    attenuation = models.FloatField(u'Общее затухание', blank=True, null=True)

    class Meta:
        verbose_name = u'Спецификация сплиттера'
        verbose_name_plural = u'Спецификации сплиттеров'
        ordering = ['title']


class SupportPoint(models.Model):
    LAYERS = Choices(('plan', u'План'), ('fact', u'Факт'), ('all', u'Все'))
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255, blank=True)
    type = models.ForeignKey('dictionaries.SupportPointType', blank=True, null=True, verbose_name=u'Тип')
    latitude = models.FloatField(u'Широта')
    longitude = models.FloatField(u'Долгота')
    comment = models.TextField(u'Комментарий', blank=True, null=True)

    layer = models.CharField(u'Слой', max_length=4, choices=LAYERS, default=LAYERS.fact)

    class Meta:
        verbose_name = u'Точка опоры'
        verbose_name_plural = u'Точки опоры'

    def __unicode__(self):
        if self.title:
            return u'%s: (%s; %s)' % (self.title, self.latitude, self.longitude)
        elif self.type:
            return u'%s (%s; %s)' % (self.type, self.latitude, self.longitude)
        else:
            return '(%s; %s)' % (self.latitude, self.longitude)

    @property
    def location(self):
        return [self.latitude, self.longitude]


class UserSettings(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='map_settings')
    zoom = models.IntegerField(default=settings.DEFAULT_USER_SETTINGS['zoom'])
    latitude = models.FloatField(default=settings.DEFAULT_USER_SETTINGS['latitude'])
    longitude = models.FloatField(default=settings.DEFAULT_USER_SETTINGS['longitude'])
    layer = models.CharField(default=settings.DEFAULT_USER_SETTINGS['layer'], max_length=10)

    @property
    def location(self):
        return [self.latitude, self.longitude]
