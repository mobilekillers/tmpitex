from rest_framework.generics import UpdateAPIView

from backend.models import UserSettings
from backend.serializers import UserSettingsSerializer

class UpdateSettingsView(UpdateAPIView):
    queryset = UserSettings.objects.all()
    serializer_class = UserSettingsSerializer

    def get_object(self):
        if hasattr(self.request.user, 'map_settings'):
            return self.request.user.map_settings
        else:
            settings = UserSettings(user=self.request.user)
            return settings
