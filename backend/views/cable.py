from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from backend.serializers import CableListSerializer, CableSerializer
from backend.models import Cable

class CableListView(ListAPIView):
    serializer_class = CableListSerializer
    queryset = Cable.objects.all()


class CableView(RetrieveUpdateDestroyAPIView):
    serializer_class = CableSerializer
    queryset = Cable.objects.all()