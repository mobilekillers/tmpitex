from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response

from backend.models import SplitterType, Splitter
from backend.serializers import SplitterTypeSerializer, SplitterSerializer

class SplitterTypeView(RetrieveAPIView):
    queryset = SplitterType.objects.prefetch_related('attenuations').all()
    serializer_class = SplitterTypeSerializer


class SplitterView(RetrieveAPIView):
    queryset = Splitter.objects.prefetch_related('type__attenuations')
    serializer_class = SplitterSerializer