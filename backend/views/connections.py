from django.db.models import Q
from django.db import transaction

from rest_framework.generics import GenericAPIView, CreateAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.mixins import DestroyModelMixin
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework import status

from backend.models import (
    SupportPoint,
    ConnectionPoint,
    Segment,
    Splitter,
    Connection,
    ConnectionUnitFiber,
    ConnectionUnitSplitter
)
from backend.serializers import ConnectionPointSerializer, SegmentsConnectionSerializer, SegmentListSerializer

import uuid
import json


class PointConnectionsView(GenericAPIView, DestroyModelMixin):
    def get(self, request, pk, *args, **kwargs):
        data = {}
        point = SupportPoint.objects.prefetch_related('connection_points').get(pk=pk)

        data['location'] = [point.latitude, point.longitude]
        data['connection'] = point.connection_points.count() > 0
        data['segments'] = SegmentListSerializer(Segment.for_point(pk), many=True).data

        connections = Connection.objects.select_related('left_unit', 'right_unit',
                                                        'left_unit__segment', 'left_unit__connectionunitfiber',
                                                        'left_unit__connectionunitsplitter',
                                                        'right_unit__segment', 'right_unit__connectionunitfiber',
                                                        'right_unit__connectionunitsplitter',
                                                        ).filter(support_point_id=pk)
        data['connections'] = list()
        for connection in connections:
            connection_data = {
                'id': connection.id,
                'id_point': connection.support_point_id,
                'cid': connection.connect_point_id,
            }
            left_unit_data = PointConnectionsView.get_connection_unit(connection.left_unit, "left")
            right_unit_data = PointConnectionsView.get_connection_unit(connection.right_unit, "right")
            connection_data.update(left_unit_data)
            connection_data.update(right_unit_data)
            data['connections'].append(connection_data)

        return Response(data)

    @staticmethod
    def get_connection_unit(unit, side):
        data = {'%s_unit' % side: unit.id}
        if hasattr(unit, 'segment'):
            data.update({
                '%s_unit_type' % side: 'segment',
                '%s_segment' % side: unit.segment.id,
            })
        elif hasattr(unit, 'connectionunitfiber'):
            data.update({
                '%s_unit_type' % side: 'fiber',
                '%s_segment' % side: uuid.UUID(unit.connectionunitfiber.segment_id),
                '%s_fiber' % side: unit.connectionunitfiber.fiber_id,
            })
        elif hasattr(unit, 'connectionunitsplitter'):
            data.update({
                '%s_unit_type' % side: 'splitter',
                '%s_splitter' % side: unit.connectionunitsplitter.splitter_id,
                '%s_splitter_input_type' % side: unit.connectionunitsplitter.type,
                '%s_splitter_input_number' % side: unit.connectionunitsplitter.number,
            })
        return data

    def delete(self, request, pk, *args, **kwargs):
        self.destroy(request, pk, *args, **kwargs)


class ConnectionPointViewset(ModelViewSet):
    serializer_class = ConnectionPointSerializer
    queryset = ConnectionPoint.objects.all()

    @detail_route(methods=['GET', 'POST'])
    def connections(self, request, pk, *args, **kwargs):
        if request.method == 'GET':
            return self.get_connections(pk)
        elif request.method == 'POST':
            try:
                data = json.loads(request.body)
                return self.save_connections(pk, data)
            except Exception as e:
                return Response(e.message, status=status.HTTP_400_BAD_REQUEST)

    def save_connections(self, id, data):
        with (transaction.atomic()):
            existing_connections = Connection.objects.select_related('left_unit', 'right_unit').filter(connect_point_id=id)
            units_id = list(existing_connections.values_list('left_unit_id', flat=True)) + \
                       list(existing_connections.values_list('right_unit_id', flat=True))

            # purge fiber
            ConnectionUnitFiber.objects.filter(pk__in=units_id).delete()

            # purge spliters
            ConnectionUnitSplitter.objects.filter(pk__in=units_id).delete()

            # purge everything!
            existing_connections.delete()
            Splitter.objects.filter(connect_point_id=id).delete()

            for splitter in data['splitters']:
                Splitter(id=splitter['splitterID'], type_id=splitter['typeID'], connect_point_id=id).save()

            for connection in data['connections']:
                units = list()
                for unit_data in [connection['left'], connection['right']]:
                    if unit_data['type'] == 'fiber':
                        unit = ConnectionUnitFiber(segment_id=unit_data['segmentID'], fiber_id=unit_data['fiberID'])
                        unit.save()
                        units.append(unit)
                    elif unit_data['type'] == 'splitter':
                        unit = ConnectionUnitSplitter(splitter_id=unit_data['splitterID'], type=unit_data['isInput'],
                                                      number=unit_data['number'])
                        unit.save()
                        units.append(unit)

                obj = Connection(connect_point_id=id, support_point_id=data['point'], left_unit_id=units[0].id,
                           right_unit_id=units[1].id)
                obj.save()

        return Response()

    def get_connections(self, pk):
        connections = Connection.objects.select_related('left_unit', 'right_unit',
                                                        'left_unit__segment', 'left_unit__connectionunitfiber',
                                                        'left_unit__connectionunitsplitter',
                                                        'right_unit__segment', 'right_unit__connectionunitfiber',
                                                        'right_unit__connectionunitsplitter',
                                                        ).filter(connect_point_id=pk)
        data = list()
        for connection in connections:
            connection_data = dict()
            left_unit_data = ConnectionPointViewset.get_connection_unit(connection.left_unit, "left")
            right_unit_data = ConnectionPointViewset.get_connection_unit(connection.right_unit, "right")
            connection_data.update(left_unit_data)
            connection_data.update(right_unit_data)
            data.append(connection_data)

        return Response(data)

    @staticmethod
    def get_connection_unit(unit, side):
        data = dict()
        if hasattr(unit, 'segment'):
            data.update({
                '%s_unit_type' % side: 'segment',
                '%s_unit_segmentID' % side: unit.segment.id,
            })
        elif hasattr(unit, 'connectionunitfiber'):
            data.update({
                '%s_unit_type' % side: 'fiber',
                '%s_unit_segmentID' % side: uuid.UUID(unit.connectionunitfiber.segment_id),
                '%s_unit_fiberID' % side: unit.connectionunitfiber.fiber_id,
            })
        elif hasattr(unit, 'connectionunitsplitter'):
            data.update({
                '%s_unit_type' % side: 'splitter',
                '%s_unit_splitterID' % side: unit.connectionunitsplitter.splitter_id,
                '%s_unit_splitterInput' % side: unit.connectionunitsplitter.type,
                '%s_unit_splitterNumber' % side: unit.connectionunitsplitter.number,
            })
        return data


class MergeSegments(CreateAPIView):
    serializer_class = SegmentsConnectionSerializer
    queryset = Connection.objects.all()


class SplitSegments(GenericAPIView):
    def post(self, request, point, id1, id2):
        segments = [id1, id2]
        Connection.objects.filter(
            Q(support_point_id=point) & Q(left_unit__in=segments) & Q(right_unit__in=segments)).delete()
        return Response()


class CanMergeSegments(GenericAPIView):
    def get(self, request, point, id1, id2):
        segments_condition = Q(left_unit_id__in=[id1, id2]) | Q(right_unit_id__in=[id1, id2])
        can_merge = Connection.objects.filter(Q(support_point_id=point) & segments_condition).count() == 0
        units = ConnectionUnitFiber.objects.filter(Q(segment_id__in=[id1, id2])).values_list('id', flat=True)
        units_condition = Q(left_unit_id__in=units) | Q(right_unit_id__in=units)
        can_merge = can_merge & (Connection.objects.filter(Q(support_point_id=point) & units_condition).count() == 0)
        return Response(can_merge)


class CanConnectSegments(GenericAPIView):
    def get(self, request, point, id1, id2):
        """
        Same as CanMergeSegment, but we only check for full connection
        """
        segments_condition = Q(left_unit_id__in=[id1, id2]) | Q(right_unit_id__in=[id1, id2])
        can_merge = Connection.objects.filter(Q(support_point_id=point) & segments_condition).count() == 0
        return Response(can_merge)
