from django.db.models import Q

from rest_framework.generics import UpdateAPIView, GenericAPIView
from rest_framework.mixins import DestroyModelMixin
from rest_framework.response import Response
from rest_framework import status
from backend.serializers import SegmentSerializer, SegmentStructureSerializer, SegmentListSerializer
from backend.models import Segment, Connection, ConnectionUnitFiber

class SegmentView(UpdateAPIView, DestroyModelMixin):
    serializer_class = SegmentSerializer
    queryset = Segment.objects.all()

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def get(self, request, pk):
        try:
            segments_queryset = Segment.objects.select_related('start_point', 'end_point', 'cable')
            segments_queryset = segments_queryset.filter(pk=pk)
            segment = SegmentListSerializer(segments_queryset, many=True)
            return Response(segment.data[0], status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)


class SegmentsStructure(GenericAPIView):
    def get(self, request, id1, id2):
        try:
            queryset = Segment.objects.select_related('cable').prefetch_related('cable__modules',
                                                                                'cable__modules__color',
                                                                                'cable__modules__fibers__color')
            segment1 = queryset.get(pk=id1)
            segment2 = queryset.get(pk=id2)
            data = {
                'segments': [
                    SegmentStructureSerializer(instance=segment1).data,
                    SegmentStructureSerializer(instance=segment2).data
                ]
            }
            return Response(data)

        except Exception as e:
            return Response(e, status=status.HTTP_404_NOT_FOUND)


