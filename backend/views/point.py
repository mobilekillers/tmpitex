import json

from rest_framework.generics import DestroyAPIView, GenericAPIView
from rest_framework.response import Response
from rest_framework import status

from backend.serializers import SupportPointSerializer, SupportPointListSerializer, PointsListSerializer
from backend.models import SupportPoint


class PointView(DestroyAPIView):
    serializer_class = SupportPointListSerializer
    queryset = SupportPoint.objects.all()

    def post(self, request, pk):
        try:
            point = json.loads(request.data.get('point'))
            SupportPoint.objects.update_or_create(pk=pk, defaults=point)
            point = SupportPointListSerializer(SupportPoint.objects.get(pk=pk))
            return Response(point.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            point = SupportPointListSerializer(SupportPoint.objects.get(pk=pk))
            SupportPoint.objects.get(pk=pk).delete()
            return Response(point.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, pk):
        try:
            point = SupportPointListSerializer(SupportPoint.objects.get(pk=pk))
            return Response(point.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)


class MovePointsView(GenericAPIView):
    queryset = SupportPoint.objects.all()
    serializer_class = PointsListSerializer

    def patch(self, request):
        try:
            points = json.loads(request.body)
            for point in points:
                SupportPoint.objects.filter(id=point['id']).update(latitude=point['latitude'],
                                                                   longitude=point['longitude'])

            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)


class ChangePointsLayerView(GenericAPIView):
    queryset = SupportPoint.objects.all()
    serializer_class = PointsListSerializer

    def patch(self, request):
        try:
            data = json.loads(request.body)
            for point_id in data['points']:
                SupportPoint.objects.filter(id=point_id).update(layer=data['layer'])

            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)
