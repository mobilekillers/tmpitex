import json

from django.db import transaction
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from django.utils.text import mark_safe

from backend.serializers import SupportPointListSerializer, SegmentListSerializer, SegmentStructureSerializer
from backend.models import SupportPoint, Segment, Connection
from dictionaries.models import SupportPointType


class RoutesView(APIView):

    def get(self, request):
        cable = request.query_params.get('cable', None)
        layer = request.query_params.get('layer', SupportPoint.LAYERS.all)
        points_queryset = SupportPoint.objects.prefetch_related('connection_points')
        segments_queryset = Segment.objects.select_related('start_point', 'end_point', 'cable')
        try:
            if layer != SupportPoint.LAYERS.all:
                points_queryset = points_queryset.filter(Q(layer=layer) | Q(layer=SupportPoint.LAYERS.all))
            points = SupportPointListSerializer(points_queryset, many=True)

            if layer != SupportPoint.LAYERS.all:
                segments_queryset = segments_queryset.filter(Q(start_point__layer=layer) |
                                                             Q(start_point__layer=SupportPoint.LAYERS.all))
                segments_queryset = segments_queryset.filter(Q(end_point__layer=layer) |
                                                             Q(end_point__layer=SupportPoint.LAYERS.all))

            if cable is not None:
                segments_queryset = segments_queryset.filter(cable__id=cable)

            segments = SegmentListSerializer(segments_queryset, many=True)

            icons = dict()
            icons['default'] = settings.ICON_SETTINGS['default']
            for type in SupportPointType.objects.all():
                if type.icon != "":
                    icons[str(type.id)] = {
                        'support': type.icon.url,
                        'connection': type.connection_icon.url,
                        'selected': type.selected_icon.url,
                        'support-transparent': type.transparent_icon.url,
                        'connection-transparent': type.transparent_connection_icon.url
                    }

            data = {
                'points': points.data,
                'segments': segments.data,
                'icons': icons
            }

            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        layer = request.data.get('layer', SupportPoint.LAYERS.fact)
        cable = request.data.get('cable', None)
        points = json.loads(request.data.get('points'))
        res_points = []
        res_segments = []


        segments_queryset = Segment.objects.select_related('start_point', 'end_point', 'cable')

        if layer != SupportPoint.LAYERS.all:
            segments_queryset = segments_queryset.filter(Q(start_point__layer=layer) |
                                                             Q(start_point__layer=SupportPoint.LAYERS.all))
            segments_queryset = segments_queryset.filter(Q(end_point__layer=layer) |
                                                             Q(end_point__layer=SupportPoint.LAYERS.all))


        if cable is not None:
            segments_queryset = segments_queryset.filter(cable__id=cable)
            #tmp_segments_queryset = segments_queryset


        with (transaction.atomic()):
            try:
                # create points
                for point_json in points:
                    point_json['layer'] = layer
                    SupportPoint.objects.update_or_create(pk=point_json['id'], defaults=point_json)
                    res_points.append(SupportPointListSerializer(SupportPoint.objects.get(pk=point_json['id'])).data)

                # if there are more than 1 point - create segments between them
                if len(points) > 1:
                    # track previous segment
                    left = None
                    for start, end in zip(points[:-1], points[1:]):
                        segment = Segment.objects.create(start_point_id=start['id'], end_point_id=end['id'],
                                                         cable_id=cable)

                        tmp_segments_queryset = segments_queryset.filter(id=segment.id)
                        res_segments.append(SegmentListSerializer(tmp_segments_queryset, many=True).data[0])
                        # create connection with previous segment
                        if left is not None:
                            connection = Connection.objects.create(support_point_id=start['id'],
                                                      left_unit_id=left.id,
                                                      right_unit_id=segment.id)
                        # track new segment
                        left = segment

            except Exception as e:
                return Response(e.message, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        data = {
            'points': res_points,
            'segments': res_segments
        }
        return Response(data, status=status.HTTP_200_OK)
