from rest_framework import serializers

from backend import models
from dictionaries import models as dictionaries


class CableListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cable
        fields = ('id', 'title')


class CableParamSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CableParameter
        fields = ('id', 'type', 'value')


class FiberSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Fiber
        fields = ('id', 'number', 'color')


class ModuleSerializer(serializers.ModelSerializer):
    fibers = FiberSerializer(many=True)

    class Meta:
        model = models.Module
        fields = ('id', 'number', 'color', 'fibers')


class CableSerializer(serializers.ModelSerializer):
    parameters = CableParamSerializer(many=True)
    modules = ModuleSerializer(many=True)

    class Meta:
        model = models.Cable
        fields = ('id', 'title', 'description', 'attenuation', 'parameters', 'modules')



class FiberStructureSerializer(serializers.ModelSerializer):
    color = serializers.SlugRelatedField(slug_field='value', read_only=True)
    class Meta:
        model = models.Fiber
        fields = ('id', 'number', 'color')


class ModuleStructureSerializer(serializers.ModelSerializer):
    fibers = FiberStructureSerializer(many=True)
    color = serializers.SlugRelatedField(slug_field='value', read_only=True)
    class Meta:
        model = models.Module
        fields = ('id', 'number', 'color', 'fibers')

class SegmentStructureSerializer(serializers.ModelSerializer):
    modules = serializers.SerializerMethodField()

    def get_modules(self, obj):
        return ModuleStructureSerializer(obj.cable.modules.all(), many=True).data

    class Meta:
        model = models.Segment
        fields = ('id', 'modules')



class SegmentSerializer(serializers.ModelSerializer):
    cable = serializers.PrimaryKeyRelatedField(queryset=models.Cable.objects.all())
    brigade = serializers.PrimaryKeyRelatedField(queryset=dictionaries.Brigade.objects.all())
    class Meta:
        model = models.Segment
        fields = ('id', 'length', 'attenuation', 'cable', 'brigade')


class SegmentListSerializer(serializers.ModelSerializer):
    cable = CableListSerializer()
    start_point = serializers.SlugRelatedField(slug_field='location', read_only=True)
    end_point = serializers.SlugRelatedField(slug_field='location', read_only=True)
    class Meta:
        model = models.Segment
        fields = ('id', 'start_point', 'end_point', 'cable')


class SupportPointSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SupportPoint
        fields = ('id', 'title', 'type', 'location', 'comment', 'layer')


class SupportPointListSerializer(serializers.ModelSerializer):
    connection = serializers.SerializerMethodField('has_connections')
    type = serializers.PrimaryKeyRelatedField(queryset=dictionaries.SupportPointType.objects.all())

    def has_connections(self, object):
        return object.connection_points.count() > 0

    class Meta:
        model = models.SupportPoint
        fields = ('id', 'title', 'type', 'location', 'comment', 'layer', 'connection')


class UserSettingsSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        instance.user = self.context['view'].request.user
        return super(UserSettingsSerializer, self).update(instance, validated_data)

    class Meta:
        model = models.UserSettings
        fields = ('zoom', 'latitude', 'longitude', 'layer')


class PointsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SupportPoint
        fields = ('id', 'latitude', 'longitude')
        #list_serializer_class = BulkListSerializer


class AttenuationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SplitterOutputAttenuation
        fields = ('id', 'output_number', 'attenuation')



class SplitterTypeSerializer(serializers.ModelSerializer):
    attenuations = AttenuationSerializer(many=True)
    class Meta:
        model = models.SplitterType
        fields = ('id', 'title', 'inputs', 'outputs', 'attenuation', 'attenuations')


class SplitterTypesListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SplitterType
        fields = ('id', 'title', 'inputs', 'outputs', 'attenuation')


class SplitterSerializer(serializers.ModelSerializer):
    type = SplitterTypeSerializer(many=False)
    class Meta:
        model = models.Splitter
        fields = ('id', 'serial', 'type')


class ConnectionPointSerializer(serializers.ModelSerializer):
    support_point = serializers.PrimaryKeyRelatedField(queryset=models.SupportPoint.objects.all())
    id = serializers.UUIDField(read_only=False)

    class Meta:
        model = models.ConnectionPoint
        fields = ('id', 'name', 'type', 'support_point')


class SegmentsConnectionSerializer(serializers.ModelSerializer):
    support_point = serializers.PrimaryKeyRelatedField(queryset=models.SupportPoint.objects.all())
    left_unit = serializers.PrimaryKeyRelatedField(queryset=models.Segment.objects.all())
    right_unit = serializers.PrimaryKeyRelatedField(queryset=models.Segment.objects.all())

    class Meta:
        model = models.Connection
        fields = ('id', 'left_unit', 'right_unit', 'support_point')