# coding: utf-8
from django.contrib import admin
from django_admin_bootstrapped.admin.models import SortableInline
from django.contrib.staticfiles.templatetags.staticfiles import static

import os
from django.conf import settings

from .models import (
    ConnectionPoint, Cable, CableParameter, Connection, ConnectionUnitFiber, ConnectionUnitSplitter,
    Fiber, Module, Segment, Splitter, SplitterOutputAttenuation, SplitterType, SupportPoint
)

class ColorDisplayMixin(object):
    def color_display(self, instance):
        return instance.color.display
    color_display.short_description = u'Цвет'
    color_display.allow_tags = True

class FiberAdmin(admin.ModelAdmin, ColorDisplayMixin):
    list_display = ['module', 'number', 'color_display']

    def get_queryset(self, request):
        return Fiber.objects.select_related('module', 'module__cable', 'color').all()

    class Media:
        css = {
            'all': ['styles/dropdown.css']
        }
        js = [  'js/jquery-ui.min.js',
                'js/color_field.js',
                'js/itex.dropdown.js']


class FiberInline(admin.TabularInline, SortableInline):
    sortable_field_name = 'number'
    model = Fiber
    fields = ['number', 'color', 'module']
    extra = 0

    @property
    def media(self):
        media = super(FiberInline, self).media
        sortable_media = SortableInline.Media
        media.add_js(sortable_media.js)
        media.add_css(sortable_media.css)
        css = {
            'all': ['styles/dropdown.css']
        }
        js = [  'js/jquery-ui.min.js',
                'js/color_field.js',
                'js/itex.dropdown.js']
        media.add_css(css)
        media.add_js(js)
        return media



class ModuleInlineAdmin(admin.TabularInline, SortableInline):
    sortable_field_name = 'number'
    model = Module
    readonly_fields = ['fiber_wise']
    fields = ['number', 'color', 'fiber_wise']
    extra = 0

    @property
    def media(self):
        media = super(ModuleInlineAdmin, self).media
        sortable_media = SortableInline.Media
        media.add_js(sortable_media.js)
        media.add_css(sortable_media.css)
        css = {
            'all': ['styles/dropdown.css']
        }
        js = [  'js/jquery-ui.min.js',
                'js/color_field.js',
                'js/itex.dropdown.js']
        media.add_css(css)
        media.add_js(js)
        return media


class ModuleAdmin(admin.ModelAdmin, ColorDisplayMixin):
    inlines = [FiberInline]
    list_display = ['cable', 'number', 'color_display']

    def get_queryset(self, request):
        return Module.objects.select_related('cable', 'color').all()

    class Media:
        css = {
            'all': ['styles/dropdown.css']
        }
        js = [  'js/jquery-ui.min.js',
                'js/color_field.js',
                'js/itex.dropdown.js']


class SegmentAdmin(admin.ModelAdmin):

    list_display = ['cable', 'start_point', 'end_point']


class CableAdmin(admin.ModelAdmin):
    inlines = [ModuleInlineAdmin]


class CableParamAdmin(admin.ModelAdmin):
    list_display = ['cable', 'type', 'value']


class SupportPointAdmin(admin.ModelAdmin):
    list_display = ['title', 'type', 'latitude', 'longitude','layer']
    list_filter = ['layer','type']


# Register your models here.
admin.site.register([
    ConnectionPoint, Connection, ConnectionUnitFiber, ConnectionUnitSplitter,
    Splitter, SplitterOutputAttenuation, SplitterType])

admin.site.register(Fiber, FiberAdmin)
admin.site.register(Cable, CableAdmin)
admin.site.register(CableParameter, CableParamAdmin)
admin.site.register(Module, ModuleAdmin)
admin.site.register(Segment, SegmentAdmin)
admin.site.register(SupportPoint, SupportPointAdmin)