# coding: utf-8

from django.apps import AppConfig

class BackendConfig(AppConfig):
    name = 'backend'
    verbose_name = u'Основное'
