# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Cables(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    attenuation = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cables'


class CablesParam(models.Model):
    id_cable = models.ForeignKey(Cables, db_column='id_cable')
    id_param = models.ForeignKey('CablesParamType', db_column='id_param')
    param_value = models.TextField()

    class Meta:
        managed = False
        db_table = 'cables_param'
        unique_together = (('id_cable', 'id_param'),)


class CablesParamType(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    title = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'cables_param_type'


class Colors(models.Model):
    color = models.CharField(primary_key=True, max_length=10)

    class Meta:
        managed = False
        db_table = 'colors'


class ConnectPoints(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    name = models.CharField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=200, blank=True, null=True)
    id_support_point = models.ForeignKey('SupportPoints', db_column='id_support_point', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'connect_points'


class Connections(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    id_point = models.CharField(max_length=36)
    left_unit = models.CharField(max_length=32)
    right_unit = models.CharField(max_length=32)
    id_connect_point = models.CharField(max_length=32, blank=True, null=True)
    left_unit_type = models.CharField(max_length=8, blank=True, null=True)
    right_unit_type = models.CharField(max_length=8, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'connections'


class ConnectionsUnitFiber(models.Model):
    id = models.CharField(primary_key=True, max_length=32)
    id_segment = models.CharField(max_length=36)
    id_fiber = models.CharField(max_length=36)

    class Meta:
        managed = False
        db_table = 'connections_unit_fiber'


class ConnectionsUnitSplitter(models.Model):
    id = models.CharField(primary_key=True, max_length=32)
    id_splitter = models.CharField(max_length=32, blank=True, null=True)
    input = models.TextField(blank=True, null=True)  # This field type is a guess.
    number = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'connections_unit_splitter'


class Fiber(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    number = models.IntegerField()
    color = models.CharField(max_length=32, blank=True, null=True)
    id_module = models.ForeignKey('Modules', db_column='id_module')

    class Meta:
        managed = False
        db_table = 'fiber'


class Modules(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    number = models.IntegerField()
    color = models.CharField(max_length=32, blank=True, null=True)
    id_cable = models.ForeignKey(Cables, db_column='id_cable')

    class Meta:
        managed = False
        db_table = 'modules'


class Segments(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    start_point = models.ForeignKey('SupportPoints', db_column='start_point', related_name='starting_segments')
    end_point = models.ForeignKey('SupportPoints', db_column='end_point', related_name='ending_segments')
    id_cable = models.CharField(max_length=36)
    length = models.FloatField(blank=True, null=True)
    attenuation = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'segments'


class Splitters(models.Model):
    id = models.CharField(primary_key=True, max_length=32)
    id_type = models.CharField(max_length=32, blank=True, null=True)
    serial = models.CharField(max_length=45, blank=True, null=True)
    id_connect_point = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'splitters'


class SplittersAttenuation(models.Model):
    id = models.CharField(primary_key=True, max_length=32)
    output_number = models.IntegerField()
    attenuation = models.FloatField()

    class Meta:
        managed = False
        db_table = 'splitters_attenuation'


class SplittersTypes(models.Model):
    id = models.CharField(primary_key=True, max_length=32)
    title = models.CharField(max_length=255)
    inputs = models.IntegerField()
    outputs = models.IntegerField()
    attenuation = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'splitters_types'


class SupportPoints(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    location = models.CharField(max_length=100)
    id_type = models.ForeignKey('SupportPointsType', db_column='id_type', blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    title = models.CharField(max_length=100)
    layer = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'support_points'


class SupportPointsType(models.Model):
    id = models.CharField(primary_key=True, max_length=36)
    title = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'support_points_type'


class Users(models.Model):
    user_id = models.CharField(primary_key=True, max_length=32)
    user_login = models.CharField(max_length=30)
    user_password = models.CharField(max_length=32)
    user_hash = models.CharField(max_length=32, blank=True, null=True)
    user_ip = models.IntegerField(blank=True, null=True)
    admin = models.TextField()  # This field type is a guess.
    zoom = models.IntegerField(blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    layer = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
