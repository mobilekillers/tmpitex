from django.core.management import BaseCommand
from django.core.exceptions import ObjectDoesNotExist

import legacy.models as legacy
import backend.models as models
import dictionaries.models as dictionaries
import json

import uuid

from django.db import transaction

class Command(BaseCommand):
    def handle(self, *args, **options):
        with (transaction.atomic()):
            print 'Migrating colors...'
            self.migrate_colors()
            print 'Migrating cable parameter types...'
            self.migrate_cable_param_types()
            print 'Migrating cables..'
            self.migrate_cable()
            print 'Migrating cable params...'
            self.migrate_cable_params()
            print 'Migrating modules...'
            self.migrate_modules()
            print 'Migrating fiber...'
            self.migrate_fiber()
            print 'Migrating splitter types...'
            self.migrate_splitter_types()
            print 'Migrating splitter attenuations...'
            self.migrate_splitter_attenuations()
            print 'Migrating support point types...'
            self.migrate_support_point_types()
            print 'Migrating support points...'
            self.migrate_support_points()
            print 'Migrating connect points...'
            self.migrate_connect_points()
            print 'Migrating segments...'
            self.migrate_segments()
            print 'Migrating splitters...'
            self.migrate_splitters()
            print 'Migrating connection units fiber...'
            self.migrate_connection_units_fiber()
            print 'Migrating connection units splitter...'
            self.migrate_connection_units_splitter()
            print 'Migrating connection...'
            self.migrate_connections()
            

    def migrate_colors(self):
        for color in legacy.Colors.objects.all():
            dictionaries.Color.objects.update_or_create(value=color.color)

    def migrate_cable_param_types(self):
        for cable_param_type in legacy.CablesParamType.objects.all():
            dictionaries.CableParameterType.objects.update_or_create(id=cable_param_type.id, name=cable_param_type.title)

    def migrate_cable(self):
        for cable in legacy.Cables.objects.all():
            models.Cable.objects.update_or_create(id=cable.id,
                                                  title=cable.title,
                                                  description=cable.description,
                                                  attenuation=cable.attenuation)

    def migrate_cable_params(self):
        for cable_param in legacy.CablesParam.objects.all():
            models.CableParameter.objects.update_or_create(cable_id=cable_param.id_cable_id,
                                                           type_id=cable_param.id_param_id,
                                                           value=cable_param.param_value)

    def migrate_modules(self):
        for module in legacy.Modules.objects.all():
            color = dictionaries.Color.objects.get(value=module.color)
            models.Module.objects.update_or_create(id=module.id, number=module.number, color=color, cable_id=module.id_cable_id)

    def migrate_fiber(self):
        for fiber in legacy.Fiber.objects.all():
            color = dictionaries.Color.objects.get(value=fiber.color)
            models.Fiber.objects.update_or_create(id=fiber.id, number=fiber.number, color=color, module_id=fiber.id_module_id)

    def migrate_splitter_types(self):
        for splitter_type in legacy.SplittersTypes.objects.all():
            models.SplitterType.objects.update_or_create(id=splitter_type.id, title=splitter_type.title,
                                                         inputs=splitter_type.inputs, outputs=splitter_type.outputs,
                                                         attenuation=splitter_type.attenuation)

    def migrate_splitter_attenuations(self):
        for split_att in legacy.SplittersAttenuation.objects.all():
            models.SplitterOutputAttenuation.objects.update_or_create(type_id=split_att.id,
                                                                      output_number=split_att.output_number,
                                                                      attenuation=split_att.attenuation)

    def migrate_support_point_types(self):
        for point_type in legacy.SupportPointsType.objects.all():
            dictionaries.SupportPointType.objects.update_or_create(id=point_type.id, title=point_type.title)

    def migrate_support_points(self):
        for point in legacy.SupportPoints.objects.all():
            location = json.loads(point.location)
            layer = "all"
            if point.layer == 1:
                layer = "fact"
            elif point.layer == 2:
                layer = "plan"

            models.SupportPoint.objects.update_or_create(id=point.id, latitude=location[0], longitude=location[1],
                                                         type_id=point.id_type_id, title=point.title,
                                                         comment=point.comment, layer=layer)

    def migrate_connect_points(self):
        for c_point in legacy.ConnectPoints.objects.all():
            models.ConnectionPoint.objects.update_or_create(id=c_point.id, name=c_point.name, type=c_point.type,
                                                            support_point_id=c_point.id_support_point_id)

    def migrate_segments(self):
        for segment in legacy.Segments.objects.all():
            models.Segment.objects.update_or_create(id=segment.id, start_point_id=segment.start_point_id,
                                                    end_point_id=segment.end_point_id, length=segment.length,
                                                    attenuation=segment.attenuation, cable_id=segment.id_cable)

    def migrate_splitters(self):
        for splitter in legacy.Splitters.objects.all():
            if models.ConnectionPoint.objects.filter(id=splitter.id_connect_point).count() == 0:
                print 'skipping splitter id %s: connection point not found' % splitter.id
                continue
            models.Splitter.objects.update_or_create(id=splitter.id, type_id=splitter.id_type, serial=splitter.serial,
                                                     connect_point_id=splitter.id_connect_point)

    def migrate_connection_units_fiber(self):
        for unit in legacy.ConnectionsUnitFiber.objects.all():
            try:
                segment = models.Segment.objects.get(pk=unit.id_segment)
                models.ConnectionUnitFiber.objects.update_or_create(id=unit.id, segment=segment, fiber_id=unit.id_fiber)
            except ObjectDoesNotExist as e:
                print 'skipping connection unit fiber with id: %s; %s' % (unit.id, e.message)


    def migrate_connection_units_splitter(self):
        for unit in legacy.ConnectionsUnitSplitter.objects.all():
            try:
                splitter = models.Splitter.objects.get(pk=unit.id_splitter)
                models.ConnectionUnitSplitter.objects.update_or_create(id=unit.id, splitter=splitter,
                                                                   type=ord(unit.input), number=unit.number)
            except ObjectDoesNotExist as e:
                print 'skipping connection unit splitter with id: %s; %s' % (unit.id, e.message)


    def migrate_connections(self):
        for connection in legacy.Connections.objects.all():
            try:
                left_unit = models.ConnectionUnit.objects.get(pk=connection.left_unit)
                right_unit = models.ConnectionUnit.objects.get(pk=connection.right_unit)
                connect_point = models.ConnectionPoint.objects.filter(pk=connection.id_connect_point).first()
                models.Connection.objects.update_or_create(id=connection.id, support_point_id=connection.id_point,
                                                       connect_point=connect_point,
                                                       left_unit=left_unit,
                                                       right_unit=right_unit)
            except ObjectDoesNotExist as e:
                print 'skipping connection between %s id %s and %s id %s: %s' %  (connection.left_unit_type,
                    connection.left_unit, connection.right_unit_type, connection.right_unit, e.message)