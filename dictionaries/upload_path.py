from django.utils.deconstruct import deconstructible

@deconstructible
class UploadPointIcon(object):
    def __init__(self, path):
        self.path = path

    def __call__(self, instance, filename):
        return (self.path % (instance.title, filename)).encode('utf-8')

