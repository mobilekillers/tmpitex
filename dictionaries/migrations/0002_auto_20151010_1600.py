# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import dictionaries.upload_path


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Brigade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0411\u0440\u0438\u0433\u0430\u0434\u0430',
                'verbose_name_plural': '\u0411\u0440\u0438\u0433\u0430\u0434\u044b',
            },
        ),
        migrations.AlterField(
            model_name='supportpointtype',
            name='icon',
            field=models.ImageField(default=b'/Users/cloud/dev/django/itex_network/static', upload_to=dictionaries.upload_path.UploadPointIcon(b'icons/%s/%s'), verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430'),
        ),
    ]
