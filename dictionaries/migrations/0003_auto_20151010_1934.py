# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import dictionaries.upload_path
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0002_auto_20151010_1600'),
    ]

    operations = [
        migrations.AlterField(
            model_name='supportpointtype',
            name='icon',
            field=imagekit.models.fields.ProcessedImageField(upload_to=dictionaries.upload_path.UploadPointIcon(b'icons/%s/%s'), verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430'),
        ),
    ]
