# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0004_auto_20151010_2004'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='supportpointtype',
            options={'ordering': ['title'], 'verbose_name': '\u0422\u0438\u043f \u0442\u043e\u0447\u043a\u0438 \u043e\u043f\u043e\u0440\u044b', 'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0442\u043e\u0447\u0435\u043a \u043e\u043f\u043e\u0440\u044b'},
        ),
    ]
