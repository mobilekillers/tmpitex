# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import dictionaries.upload_path
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CableParameterType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440 \u043a\u0430\u0431\u0435\u043b\u044f',
                'verbose_name_plural': '\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b \u043a\u0430\u0431\u0435\u043b\u044f',
            },
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(unique=True, max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0426\u0432\u0435\u0442',
                'verbose_name_plural': '\u0426\u0432\u0435\u0442\u0430',
            },
        ),
        migrations.CreateModel(
            name='SupportPointType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('icon', models.ImageField(default=b'D:\\work\\django\\itex_network\\static', upload_to=dictionaries.upload_path.UploadPointIcon(b'icons/%s/%s'), verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0442\u043e\u0447\u043a\u0438 \u043e\u043f\u043e\u0440\u044b',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0442\u043e\u0447\u0435\u043a \u043e\u043f\u043e\u0440\u044b',
            },
        ),
    ]
