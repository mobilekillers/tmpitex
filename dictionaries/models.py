# coding: utf-8

from django.db import models
from imagekit.models import ProcessedImageField, ImageSpecField
from django.conf import settings
import uuid
from core.tools.imagespecs import (MapIconSupport, MapIconConnection, MapIconSelected, MapIconSupportTransparent, MapIconConnectionTransparent)
from django.utils.text import mark_safe

class CableParameterType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, verbose_name=u'Название', unique=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Параметр кабеля'
        verbose_name_plural = u'Параметры кабеля'


class Color(models.Model):
    value = models.CharField(u'Значение', unique=True, max_length=255)

    def __unicode__(self):
        return self.value

    @property
    def display(self):
        parts = self.value.split("#")
        html = '<div style="position: relative">' \
               '<span style="width:20px; height:20px; border: 1px solid white; ' \
               'border-radius: 10px; display:inline-block; background-color:#%s">' \
               '</span>' % parts[1]
        if parts[0] == '1':
            html += u'<span style="position:absolute; left: 22px">+кольцо</span>'
        html += "</div>"
        return mark_safe(html)

    class Meta:
        verbose_name = u'Цвет'
        verbose_name_plural = u'Цвета'


class SupportPointType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(u'Название', max_length=255)
    icon = ProcessedImageField(verbose_name=u'Иконка', spec=MapIconSupport, upload_to='icons', autoconvert=None)
    connection_icon = ImageSpecField(spec=MapIconConnection, source='icon')
    selected_icon = ImageSpecField(spec=MapIconSelected, source='icon')
    transparent_icon = ImageSpecField(spec=MapIconSupportTransparent, source='icon')
    transparent_connection_icon = ImageSpecField(spec=MapIconConnectionTransparent, source='icon')

    class Meta:
        verbose_name = u'Тип точки опоры'
        verbose_name_plural = u'Типы точек опоры'
        ordering = ['title']

    def __unicode__(self):
        return self.title

    @staticmethod
    def as_list():
        return SupportPointType.objects.all().values('id', 'title')




class Brigade(models.Model):
    title = models.CharField(u'Название', max_length=255)

    class Meta:
        verbose_name = u'Бригада'
        verbose_name_plural = u'Бригады'

    def __unicode__(self):
        return self.title