# coding: utf-8

from django.contrib import admin
from django.utils.text import mark_safe
from .models import (CableParameterType, Color, SupportPointType, Brigade)
from collections import OrderedDict


class SupportPointTypeAdmin(admin.ModelAdmin):
    readonly_fields =  ['iconset']
    fields = ['title', 'icon', 'iconset']
    def iconset(self, instance):
        links = list()
        for (name, url) in [
            (u'Точка опоры', instance.icon.url),
            (u'Точка соединения', instance.connection_icon.url),
            (u'Выбрана (в режиме построения маршрута)', instance.selected_icon.url),
            (u'Исходное положение точки опоры при перемещении', instance.transparent_icon.url),
            (u'Исходное положение точки соединения при перемещении', instance.transparent_connection_icon.url)
            ]:
            links.append("<img src='%s' /> - %s<br/>" % (url, name))

        return mark_safe("".join(links))

    iconset.short_description = u"Набор иконок"


admin.site.register([CableParameterType, Color, Brigade])
admin.site.register(SupportPointType, SupportPointTypeAdmin)