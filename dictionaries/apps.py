# coding: utf-8

from django.apps import AppConfig

class DictionaryConfig(AppConfig):
    name = 'dictionaries'
    verbose_name = u'Справочники'