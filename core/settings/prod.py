from base import *

DEBUG = False

ALLOWED_HOSTS = ['bs.itex.su', 'network.itex.su']

WSGI_APPLICATION = 'core.dev_wsgi.application'

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

from local_db import *
DB_NO_OPTIMIZE

