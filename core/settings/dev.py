from .base import *

# INSTALLED_APPS += (
#     'debug_toolbar',
# )

DEBUG = True

WSGI_APPLICATION = 'core.dev_wsgi.application'

ALLOWED_HOSTS = ['*']

from local_db import *
DB_NO_OPTIMIZE