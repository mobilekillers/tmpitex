# coding: utf-8

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'n_sseuc*u7h+p(t(x*zfg2nbu(bslz_dzmoakp8#+&3-q%d2d+'

# Application definition

INSTALLED_APPS = (
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework_swagger',

    'bootstrap3',
    'imagekit',

    'backend',
    'frontend',
    'dictionaries',
    'legacy'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'frontend.middleware.RequireLoginMiddleware',
)

ROOT_URLCONF = 'core.urls'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
    },
    'staticfiles': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'staticfiles-filehashes'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TIME_ZONE = 'Europe/Moscow'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'backend/static'),
)

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'backend.middleware.SessionCsrfExemptAuthentication'
    )
}

from misc import *

LOCALE_PATHS = (os.path.join(BASE_DIR, os.path.join('django_admin_bootstrapped', 'locale')),
                os.path.join(BASE_DIR, os.path.join('backend', 'locale')))

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_REQUIRED_URLS = (
    r'/(.*)$',
)
LOGIN_REQUIRED_URLS_EXCEPTIONS = {
    LOGIN_URL,
    r'/admin/(.*)'
}

SWAGGER_SETTINGS = {
    'info': {
        'license': 'Apache 2.0',
        'licenseUrl': 'http://www.apache.org/licenses/LICENSE-2.0.html',
    },
    'doc_expansion': 'list',
    'enabled_methods': ['get', 'post', 'put', 'delete'],
}


# Icons processing

ICON_SETTINGS = {
    'default': {
      'support': os.path.join(STATIC_URL, 'images/map/default-support.png'),
      'connection': os.path.join(STATIC_URL, 'images/map/default-connection.png'),
      'selected': os.path.join(STATIC_URL, 'images/map/default-selected.png'),
      'support-transparent': os.path.join(STATIC_URL, 'images/map/default-support-transparent.png'),
      'connection-transparent': os.path.join(STATIC_URL, 'images/map/default-connection-transparent.png')
    },
    'support': {'color': "#00569C"},
    'connection': {'color': "#FF532D"},
    'selected': {'color': "#FF9800"},
    'transparent_alpha': 0.5
}