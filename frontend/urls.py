from django.conf.urls import patterns, url, include

from .views import Index, PointView, SegmentView, ConnectionView


auth_urls = patterns('',
                     url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
                     url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout')
                     )

urlpatterns = patterns('',
                       url(r'^', include(auth_urls)),
                       url(r'^$', Index.as_view()),
                       url(r'^point$', PointView.as_view()),
                       url(r'^segment/(?P<pk>[^/]+)/$', SegmentView.as_view()),
                       url(r'^connection$', ConnectionView.as_view())
                       )

