<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/json.js" type='text/javascript'></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/util_script.js" type="text/javascript"></script>
<script src="js/jquery.dataTables.min.js" type='text/javascript'></script>
<script src="js/jquery.editable.js" type='text/javascript'></script>
<link href="css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
<link href="css/styles.css" rel="stylesheet" type="text/css"/>
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
var splitterID = null;

function ShowSplitterInfo(id) {
    splitterID = id;

    $.ajax({
        method: "POST",
        url: "include/splitter_controller.php",
        data: {
            op: "get_splitter_type_info",
            id: id
        }
    }).done(function (response) {
        var result = JSON.parse(response);
        $("#spnTitle").text(result.splitter.title);
        $("#spnInputs").text(result.splitter.inputs);
        $("#spnOutputs").text(result.splitter.outputs);

        $("#tblNonUniformAttenuation tbody").html("");

        if (result.splitter.attenuation) {
            $("#ddlAttenuationType").val("Равномерное");
            $("#spnUniformAttenuation").text(result.splitter.attenuation);
        }
        else {
            $("#ddlAttenuationType").val("Неравномерное");
            $("#spnUniformAttenuation").text("");

            if (result.attenuations) {
                CreateAttenuationTable(result.splitter.outputs);

                for (var i = 0; i < result.attenuations.length; i++) 
                    $("#tblNonUniformAttenuation tbody tr").eq(result.attenuations[i].output_number - 1).find("span.editable_single").text(result.attenuations[i].attenuation);
            }
        }

        DdlAttenuationType_SelectedIndexChanged(false);

        $("#divSplitterForm").dialog("open");
    });
}

function CreateAttenuationTable(outputNumber) {
    for (var i = 1; i < parseInt(outputNumber) + 1; i++) {
        $("#tblNonUniformAttenuation tbody").append("<tr><td>" + i + "</td><td><span class='editable_single'>0</span></td></tr>");
    }
}

function DdlAttenuationType_SelectedIndexChanged(clearAttenuationTable) {
    $("#ddlAttenuationType option:selected").each(function () {
        var tbl = $("#tblNonUniformAttenuation");
        var spnStaticAttenuation = $("#spnUniformAttenuation");

        if ($(this).text() == "Равномерное") {
            tbl.hide();
            spnStaticAttenuation.show();
            $("#tblNonUniformAttenuation tbody").html("");
        }
        else {
            tbl.show();
            spnStaticAttenuation.hide();

            if (clearAttenuationTable) {
                $("#tblNonUniformAttenuation tbody").html("");
				$("#spnOutputs").editable("close");
                var outputs = $("#spnOutputs").text();
                CreateAttenuationTable(parseInt(outputs));
				$("#tblNonUniformAttenuation span.editable_single").editable();
            }
        }
    });

    
}

function RemoveSplitter() {
    if (splitterID) {
        if (confirm("Вы уверены, что хотите удалить сплиттер?")) {
            $.ajax({
                method: "POST",
                url: "include/splitter_controller.php",
                data: {
                    id: splitterID,
                    op: "del_type"
                }
            }).done(function (msg) {
                var result = JSON.parse(msg);
                if (!result.success) {
                    alert("Ошибка при удалении сплиттера: " + result.error);
                }
                else {
                    $("#divSplitterForm").dialog("close");
                    location.reload();
                }
            });
        }
    }
}

function ValidateForm()
{
    if(IsEmptyField($("#spnTitle").text()))
    {
        window.alert("Поле \"Название\" обязательно для заполнения.");
        return false;
    }

    if(IsEmptyField($("#spnInputs").text()))
    {
        window.alert("Поле \"Входы\" обязательно для заполнения.");
        return false;
    }

    if(!isInt($("#spnInputs").text()))
    {
        window.alert("Поле \"Входы\" должно принимать целочисленное значение.");
        return false;
    }

    if(IsEmptyField($("#spnOutputs").text()))
    {
        window.alert("Поле \"Выходы\" обязательно для заполнения.");
        return false;
    }

    if(!isInt($("#spnOutputs").text()))
    {
        window.alert("Поле \"Выходы\" должно принимать целочисленное значение.");
        return false;
    }

    if($("#ddlAttenuationType option:selected").text() == "Равномерное" && IsEmptyField($("#spnUniformAttenuation").text()))
    {
        window.alert("Поле \"Затухание\" обязательно для заполнения.");
        return false;
    }

    if($("#ddlAttenuationType option:selected").text() == "Равномерное" && (!isInt($("#spnUniformAttenuation").text()) && !IsFloat($("#spnUniformAttenuation").text())))
    {
        window.alert("Поле \"Затухание\" должно принимать целочисленное или вещественное значение.");
        return false;
    }

    var isEmpty = false;
    var isNotIntegerOfFloat = false;
    if($("#ddlAttenuationType option:selected").text() == "Неравномерное")
    {
        $("#tblNonUniformAttenuation tbody tr span.editable_single").each(function () {
            if(IsEmptyField($(this).text()))
            {
                isEmpty = true;
                return false;
            }

            if(!isInt($(this).text()) && !IsFloat($(this).text()))
            {
                isNotIntegerOfFloat = true;
                return false;
            }
        });
    }

    if(isEmpty)
    {
        window.alert("Поле \"Затухание\" обязательно для заполнения.");
        return false;
    }

    if(isNotIntegerOfFloat)
    {
        window.alert("Поле \"Затухание\" должно принимать целочисленное или вещественное значение.");
        return false;
    }

    return true;
}

function IsEmptyField(value)
{
    if(value == "" || value == "Не задано")
    {
        return true;
    }
    else
    {
        return false;
    }
}


function SaveSplitter() {
    if(!ValidateForm())
    {
        return;
    }

    var data = {};
    if (splitterID) {
        data.id = splitterID;
    }
    else {
        data.id = guid();
    }
    data.title = $("#spnTitle").text();
    data.inputs = $("#spnInputs").text();
    data.outputs = $("#spnOutputs").text();
    if ($("#ddlAttenuationType option:selected").text() == "Равномерное") {
        data.attenuation = $("#spnUniformAttenuation").text();
    }
    else {
        data.attenuation = null;
        var arr = [];
        $("#tblNonUniformAttenuation tbody tr span.editable_single").each(function (index) {
            arr.push({output_number: index + 1, attenuation: $(this).text()});
        });
        data.attenuationValues = arr;
    }

    $.ajax({
        method: "POST",
        url: "include/splitter_controller.php",
        data: {
            op: "update_type",
            data: JSON.stringify(data)
        }
    }).done(function (msg) {
        var result = JSON.parse(msg);
        if (result.success) {
            $("#divSplitterForm").dialog("close");
            location.reload();
        }
        else {
            window.alert(result.error);
        }
        splitterID = null;
    });
}

function DataBindSplitters()
{
    $("#splitter_table").dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {
            "sEmptyTable": "Нет элементов для отображения"
        },
        "aoColumns": [
            {
                "sClass": "ui-widget-content no-background"
            },
            {
                "sClass": "ui-widget-content no-background"
            },
            {
                "sClass": "ui-widget-content no-background"
            },
            {
                "sClass": "ui-widget-content no-background"
            }
        ],
        "aaSorting": [
            [2, "asc"]
        ]

    });
}

$(document).ready(function () {
    $("button").button();

    $("#new_splitter").click(function () {
        splitterID = null;

        $("#divSplitterForm span.editable_single").each(function () {
            $(this).text("");
        });
        $("#tblNonUniformAttenuation tbody").html("");
        $("#ddlAttenuationType").val("Равномерное");
        DdlAttenuationType_SelectedIndexChanged(false);

        $("#divSplitterForm").dialog("open");
    });

    $("#divSplitterForm").dialog({
        autoOpen: false,
        width: "auto",
        height: "auto",
        modal: true,
        title: "Редактирование сплиттера",
        buttons: {
            "Сохранить": function () {
                SaveSplitter();
            },
            "Удалить": function () {
                RemoveSplitter();
            },
            "Закрыть": function () {
                $(this).dialog("close");
            }
        }
    })

    DataBindSplitters();

    $("span.editable_single").editable();
});
</script>
</head>
<body>
		<span style="display:none"> <?php
            require_once("include/splitter_controller.php");
            require_once("include/check.php");
            $splitters = SplitterController::GetSplitterTypesArray();
            ?>
		</span>
        <table width="100%">
            <tr><td width="15%" valign="top">

                    <?php include ("include/menu.php"); ?>
                </td><td width="70%" align="center"  valign="top">
        <div class="main">
            <table id="splitter_table">
                <thead>
                <th width="40%">Название</th>
                <th width="15%">Входы</th>
                <th width="15%">Выходы</th>
                <th></th>
                </thead>
                <tbody>
                <?php foreach ($splitters as $splitter) :
                    ?>
                    <tr data-id="<?= $splitter->id ?>">
                        <td><?= $splitter->title ?></td>
                        <td><?= $splitter->inputs ?></td>
                        <td><?= $splitter->outputs ?></td>
                        <td><a href='' onclick="ShowSplitterInfo('<?= $splitter->id ?>'); return false;" class="delete">Изменить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <br/>
            <button id="new_splitter">
                Добавить сплиттер
            </button>
        </div>
        <div id="divSplitterForm">
            <table>
                <tr>
                    <td>
                        <span>Название:</span>
                        <span style="color: #ff0000">*</span>
                    </td>
                    <td>
                        <span id="spnTitle" class="editable_single"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>Входы:</span>
                        <span style="color: #ff0000">*</span>
                    </td>
                    <td>
                        <span id="spnInputs" class="editable_single"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>Выходы:</span>
                        <span style="color: #ff0000">*</span>
                    </td>
                    <td>
                        <span id="spnOutputs" class="editable_single" onchange="DdlAttenuationType_SelectedIndexChanged(true);"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>Затухание:</span>
                        <span style="color: #ff0000">*</span>
                    </td>
                    <td>
                        <span>Тип:</span>
                        <select id="ddlAttenuationType" onchange="DdlAttenuationType_SelectedIndexChanged(true);">
                            <option value="Равномерное">Равномерное</option>
                            <option value="Неравномерное">Неравномерное</option>
                        </select>
                        <span id="spnUniformAttenuation" class="editable_single" style="display: block;"></span>
                        <table id="tblNonUniformAttenuation" style="display: none">
                            <thead>
                            <tr>
                                <th>Выход</th>
                                <th>Затухание</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        </td>
        <td width="15%"></td>
        </tr>
        </table>
</body>
</html>