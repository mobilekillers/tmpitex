﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script src="js/jquery.min.js" type="text/javascript"></script>
		<script src="js/json.js" type='text/javascript'></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/util_script.js" type="text/javascript"></script>
		<link href="css/styles.css" rel="stylesheet" type="text/css" />
		<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			$(document).ready(function() {
				$("#content").dialog({
					autoOpen : false,
					width : 850,
					height : 600,
					title : 'Информация о кабеле',
				});

				$("#new-cable").click(function() {
					openDialog();
				});
				$("#cable_links").on("click", "a", function() {
					openDialog($(this).data("id"));
				});

				$("button").button();
				$("body").bind("cableSaved", cableSaved);
				$("body").bind("cableDeleted", cableDeleted);
			});

			function cableSaved() {
				$("#content").dialog("close");
				$("#cable_saved").show();
				setTimeout("$('#cable_saved').fadeOut()", 2000);
				bindCableList();
			}

			function cableDeleted() {
				$("#content").dialog("close");
				bindCableList();
			}

			function openDialog(id) {
				var src = "cable_form.php" + ( typeof id == "undefined" ? "" : ("?id=" + id));
				$('#content iframe').attr("src", src);
				$("#content").dialog("open");
			}

			function bindCableList() {
				$.ajax({
					method : "POST",
					url : "include/cable_controller.php",
					data : {
						op : "list"
					}
				}).done(function(response) {
					$cables = JSON.parse(response);
					$("#cable_links").html("");
					$.each($cables, function(id, title) {
						$("#cable_links").append("<a href='javascript:void(0)' data-id='" + id + "'>" + title + "</a><br />");
					});
				});
			}
		</script>
	</head>
	<body>
	<span style="display:none">
		
		<?php
		require_once ("include/cable_controller.php");
		require_once ("include/check.php");
		$cables = CableController::GetCablesListArray();
		
	?>
	</span>	
	    <table width="100%">
            <tr><td width="15%" valign="top">

		<?php include ("include/menu.php"); ?>
                </td><td width="70%" align="center"  valign="top">
		<div class="main">
			<div id="cable_links">
				<?php foreach ($cables as $key => $value) : ?>
					<a href='javascript:void(0)' data-id='<?= $key ?>'><?= $value ?></a><br />
				<?php endforeach; ?>
			</div>
			<br/>
			<button id="new-cable">Добавить кабель</button>
			<div id="content">
				<iframe src="" frameborder="no" width="825" height="530"></iframe>
			</div>
			<br/>
				<span style='color:gray; display:none' id="cable_saved"><i><b>Кабель сохранен!</b></i></span>
			
		</div>
                </td>
                <td width="15%"></td>
            </tr>
        </table>
	</body>
</html>