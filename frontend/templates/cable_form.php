<?php
require_once ($_SERVER["DOCUMENT_ROOT"] ."/include/config.php");
require_once("include/dict.php");
$dict = new DictionaryController($config);
$colors = $dict -> GetDictionary("colors", false);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script src="js/jquery.min.js" type="text/javascript"></script>
		<script src="js/json.js" type='text/javascript'></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/jquery.editable.js" type="text/javascript"></script>
		<script src="js/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="js/util_script.js" type="text/javascript"></script>
		<script src="js/cable.js" type='text/javascript'></script>
		<script src="js/colorpicker.js" type="text/javascript"></script>
		<script src="js/itex.dropdown.js" type='text/javascript'></script>
		<link href="css/dropdown.css" rel="stylesheet" type="text/css" />
		<link href="css/colorpicker.css" rel="stylesheet" type="text/css" />
		<link href="css/styles.css" rel="stylesheet" type="text/css" />
		<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		var cableId =<?php if (isset($_GET["id"])) echo "'".$_GET["id"]."'"; else echo "null" ?>	;
		var colors = [ <?php $cols = array(); foreach ($colors as $color) $cols[] = "'$color'"; echo join(', ', $cols); ?> ];
			$(document).ready(function() {
				$("button").button();
				$("#save-cable").click(saveCable);
				$("#delete-cable").click(deleteCable);
				if (cableId) {
					$.ajax({
						method : "POST",
						url : "include/cable_controller.php",
						data : {
							op : "get",
							id : cableId
						}
					}).done(function(response) {
						initiateCable(JSON.parse(response));
					});
				} else {
					initiateCable();
				}
				

				$("#fiber").on("click", ".fiber-delete-link", function() {
					if (confirm("Вы действительно хотите удалить волокно?")) {
						var tr = $(this).closest("tr");
						if (tr) {
							$("#fiber").dataTable().fnDeleteRow(tr.get(0));
						}
					}
					return false;
				});

				$("#modules").on("click", ".module-edit-link", function() {
					var tr = $(this).closest("tr");
					editModule(tr.attr("id"));
					return false;
				});
				
				$("#modules").on("click", ".module-copy-link", function() {
					var tr = $(this).closest("tr");
					cloneModule(tr.attr("id"));
					return false;
				});

				$("#addFiber").click(function() {
					var oTable = $('#fiber').dataTable();
					var count = parseInt($("#fiberCount").val());
					var idx = getMaxIndex($("#fiber"), 0);
					for (var i = 0; i < count; i++) {
						oTable.fnAddData([idx++, "0#ffffff", guid()]);
					}
				});

				$("#addModule").click(function() {
					var oTable = $('#modules').dataTable();
					clearModuleForm();
					$("#module-number").val(getMaxIndex($("#modules"), 0));
					$("#module-edit-form").dialog("open");
				});


				initiateModuleDialog();
				initiateTables();
			});
			

		</script>
	</head>
	<body style="font-family: Verdana, sans-serif; font-size: 80%; ">
		<input type="hidden" id="cable-id" />
		<b>Наименование: </b>&nbsp;<span id="cable-title" class="editable_single"></span>
		<br/>
		<br/>
		<b>Коэффициент затухания (дБ/км):</b>&nbsp;<span id="cable-attenuation" class="editable_single"></span>
		<br /><br />
		<b>Параметры: </b>
		<br/>
		<table id="params">
			<thead>
				<tr>
					<th width="50%">Параметр</th>
					<th>Значение</th>
				</tr>
			</thead>
		</table>
		<b>Описание: </b>
		<br/>
		<span id="cable-desc"></span><br/>
		<b>Список модулей</b>
		<br/>
		<br/>
		<table id="modules">
			<thead>
				<tr>
					<th width="15%">№</th>
					<th width="25%">Волокна</th>
					<th width="25%">Цвет</th>
					<th width="35%"></th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		<br />
		<button id="addModule" style="padding:2px">
			Добавить модуль
		</button>
		<br/>
		<br/>
		<hr/>
		<br/>
		<table width="100%">
			<tr>
				<td align="left">
				<button id="delete-cable">
					Удалить
				</button></td>
				<td align="right">
				<button id="save-cable">
					Сохранить
				</button></td>
			</tr>
		</table>
		</div>
		<div id="module-edit-form">
			<input type="hidden" value="" id="module-id" />
			<b>Номер:</b>
			<br/>
			<input id="module-number" type='text' value='1' />
			<br/>
			<br/>
			<b>Цвет:</b>
			<br/>
			<br/>
			<div id="module-color"></div>
			<br/>
			<br/>
			<b>Тип:</b>
			<br/>
			<input type='hidden' id='moduleID' />
			<table id="fiber" width="100%">
				<thead>
					<tr>
						<th width="25%">№</th>
						<th width="50%">Цвет</th>
						<th></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<br />
			Добавить:&nbsp;<input type='text' id='fiberCount' value='1' style='width:30px' />&nbsp; 
			<button id="addFiber" style="padding:2px">
				Добавить волокна
			</button>
	</body>
</html>