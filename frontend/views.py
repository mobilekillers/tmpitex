from django.views.generic import TemplateView
from rest_framework.renderers import JSONRenderer
from django.utils.text import mark_safe

from backend.models import UserSettings, SupportPoint, Segment, Cable, SplitterType
from backend.utils import geocalc
from backend.serializers import SplitterTypesListSerializer
from dictionaries.models import SupportPointType, Brigade


# Create your views here.
class Index(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['support_types'] = SupportPointType.as_list()
        context['cables'] = Cable.as_list()
        if not hasattr(self.request.user, 'map_settings'):
            UserSettings(user=self.request.user).save()
        context['settings'] = self.request.user.map_settings

        return context


class PointView(TemplateView):
    template_name = 'point_form.html'

    def get_context_data(self, **kwargs):
        context = super(PointView, self).get_context_data(**kwargs)
        id = self.request.GET.get('id', '')

        context.update({
            'id': id,
            'location': self.request.GET.get('location', 'null'),
            'layer': self.request.GET.get('layer', 'null'),
            'support_types': SupportPointType.as_list()
        })

        if id:
            point = SupportPoint.objects.select_related('type').prefetch_related('connection_points').get(
                pk=id)
            context['point'] = point
            context['layer'] = point.layer



        return context


class SegmentView(TemplateView):
    template_name = 'segment_form.html'

    def get_context_data(self, **kwargs):
        context = super(SegmentView, self).get_context_data(**kwargs)

        segment = Segment.objects.select_related('cable', 'start_point', 'end_point').get(pk=kwargs['pk'])

        context['length'] = geocalc(segment.start_point.latitude, segment.start_point.longitude,
                                    segment.end_point.latitude, segment.end_point.longitude)

        if segment.cable.attenuation:
            attenuation = context['length'] * segment.cable.attenuation
            context['attenuation'] = attenuation

        context.update({
            'segment': segment,
            'cables': Cable.objects.all().values('id', 'title'),
            'brigades': Brigade.objects.all().values('id', 'title')
        })

        return context


class ConnectionView(TemplateView):
    template_name = "connection.html"

    def get_context_data(self, **kwargs):
        context = super(ConnectionView, self).get_context_data(**kwargs)
        id = self.request.GET.get('id')
        cid = self.request.GET.get('cid', '')
        context['isNew'] = self.request.GET.get('isNew', '')
        context['isCommonSchema'] = self.request.GET.get('isCommonSchema', '')
        context['canSaveConnection'] = self.request.GET.get('canSaveConnection', '')
        point = SupportPoint.objects.get(pk=id)
        if cid:
            context['conPoint'] = point.connection_points.filter(pk=cid).first()

        context['point'] = point

        splitters = SplitterTypesListSerializer(SplitterType.objects.all(), many=True)
        # oh, extra request, but I don't know how to fix it
        context['splitters'] = SplitterType.objects.all().values('id', 'title')
        context['splitters_json'] = mark_safe(JSONRenderer().render(splitters.data))

        return context
