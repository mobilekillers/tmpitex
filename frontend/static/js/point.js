$(document).ready(function () {
    $("#add_connection").click(function () {
        parent.$("body").trigger({
            type: "connection",
            id: pointId,
            cid: null,
            create: true,
            canSaveConnection: true
        });
    });
    $("#connection_points").on("click", ".connect-edit-link", function () {
        var id = $(this).closest("tr").data("id");
        parent.$("body").trigger({
            type: "connection",
            id: pointId,
            cid: id,
            canSaveConnection: true
        });

        return false;
    });
    $("#connection_designer").on('click', function () {
        var btn = $(this).button('loading');
        parent.$("body").trigger({
            type: "connection",
            id: pointId,
            canSaveConnection: false
        });
        setTimeout(function() { btn.button('reset') }, 1000);
    });

    $("#point-title").editable();
    $("#point-comment").editable();

    $("#save-point").click(savePoint);
    $("#delete-point").click(deletePoint);

    $("body").bind("connection", function (e) {
        openConnection(e.id, e.cid ? e.cid : null, e.create ? true : false, e.canSaveConnection ? true : false);
    });

    $("body").bind("connectionSaved", function (e) {
        if (e.close)
            $("#connection_container").dialog("close");
        $('#point_container iframe').attr('src', function (i, val) {
            return val;
        });
    });
});

function savePoint() {
    if (!pointId)
        pointId = guid();

    var layer = "all";
    if (!$("#planCheck").prop("checked")) {
        layer = "fact";
    }
    else if (!$("#factCheck").prop("checked")) {
        layer = "plan";
    }

    var point = {
        comment: getValue("#point-comment"),
        title: getValue("#point-title"),
        latitude: pointLocation[0],
        longitude: pointLocation[1],
        type_id: $("#point-support-type").val(),
        layer: layer
    };

    $.post("/api/point/" + pointId + "/",  {
            point: JSON.stringify(point)
        }
    ).done(function (data) {
            parent.$("body").trigger("pointSaved",[data]);
        }).fail(function (err) {
            alert("Ошибка при сохранении: " + err.responseText);
        });
}

function deletePoint() {
    if (pointId && confirm("Вы действительно хотите удалить опорную точку?")) {
        $.ajax({
            method: "DELETE",
            url: "/api/point/" + pointId + "/"
        }).done(function (data) {
            parent.$("body").trigger("pointDeleted",[data]);
        }).fail(function(err) {
            alert("Ошибка при удалении точки: " + err.responseText);
        });
    }
}