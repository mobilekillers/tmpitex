function saveSegment() {
    if (!segmentId)
        segmentId = guid();
    var segment = {
        "length": getDouble("#segment-factlength"),
        "attenuation": getDouble("#segment-factattenuation"),
        "id": segmentId,
        "cable": $("#segment-cable").val(),
        "brigade": $("#segment-brigade").val(),
    };

    $.ajax({
        method: "PUT",
        url: "/api/segment/" + segmentId + "/",
        data: segment
    }).done(function () {
        parent.$("body").trigger("segmentSaved",[segmentId]);
    }).fail(function (err) {
        alert("Ошибка при сохранении: " + err.responseText);
    });
}

function deleteSegment() {
    if (segmentId && confirm("Вы действительно хотите удалить сегмент?")) {
        $.ajax({
            method: "DELETE",
            url: "/api/segment/" + segmentId + "/"
        }).done(function () {
                parent.$("body").trigger("segmentDeleted",[segmentId]);
        }).fail(function(err) {
                alert("Ошибка при удалении сегмента: " + err.responseText);
        });
    }
}