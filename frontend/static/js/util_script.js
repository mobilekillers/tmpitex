/**
 * @author cloud
 */


$.fn.loading = function (state, addClass) {
    var $this = $(this);
    state = state === undefined ? true : !!state;

    $this.each(function (i, element) {

        var $element = $(element);

        if (state && $element.find(".js-loading-overlay").length === 0) {
            var $overlay = $("<div/>").addClass("js-loading-overlay");
            if (addClass !== undefined) {
                $overlay.addClass(addClass);
            }

            $element.append($overlay).addClass("js-loading");
            $overlay.stop().hide().fadeIn("fast");
            $this.find("input,button,.btn").addClass("disabled").prop("disabled", true);
        } else if (!state) {
            $element.removeClass("js-loading").find(".js-loading-overlay").remove();
            $this.find("input,button,.btn").removeClass("disabled").prop("disabled", false);
        }

    });

    return this;

};

function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    }).split('-').join("");
}

function bindColorPicker($target, color) {
    if (!color) color = "#000000";
    $target.css("background-color", color);
    $target.ColorPicker({
        color: color,
        onShow: function (colpkr) {
            $(colpkr).fadeIn("fast");
            return false;
        },
        onHide: function (colpkr) {
            $(colpkr).fadeOut("fast");
            return false;
        },
        onChange: function (hsb, hex, rgb) {
            $target.css('backgroundColor', '#' + hex);
            $target.trigger("colorChange");
        }
    });
}


Array.prototype.compare = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0; i < this.length; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].compare(array[i]))
                return false;
        } else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}

function getCPColor(target) {
    return '#' + ($('#' + $(target).data('colorpickerId')).data('colorpicker').fields[0].value)

}

function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }

    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function union_arrays(x, y) {
    var obj = {};
    for (var i = x.length - 1; i >= 0; --i)
        obj[x[i]] = x[i];
    for (i = y.length - 1; i >= 0; --i)
        obj[y[i]] = y[i];
    var res = []
    for (var k in obj) {
        if (obj.hasOwnProperty(k))  // <-- optional
            res.push(obj[k]);
    }
    return res;
}

