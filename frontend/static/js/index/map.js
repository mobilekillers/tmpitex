/**
 * Created by cloud on 21.09.15.
 */

/// MARK: properties
var selectionPolygon = null;
var map;
var mapId = "map";
var cursor;
var line;
var state;
var tempPoint = null;
var movedPoints = null;
var highlightedObjects = null;
var route_from = null, route_to = null;
var ICONS = null;

/// MARK: events

ymaps.ready(function () {
    var template = ymaps.templateLayoutFactory.createClass(
        '<div class="icon_container" style="position: absolute; left: -$[properties.radius]px; top: -$[properties.radius]px; width: $[properties.size]px; height: $[properties.size]px;">' +
        '<img class="icon_img" src="$[properties.url]" style="display:block; max-width: 100%; max-height:100%"></div>',
        {
            build: function () {
                this.constructor.superclass.build.call(this);
                this._$element = $('.icon_container', this.getParentElement());
                var $this = this;
                this._$element.bind("shapechange", function() {
                    $this.events.fire('shapechange');
                });
            },
            getShape: function () {
                result = new ymaps.shape.Circle(
                    new ymaps.geometry.pixel.Circle(
                        [0, 0],
                        this._$element[0].offsetWidth / 2.0
                    )
                );

                return result;
            },
            onSublayoutSizeChange: function () {
                console.log('qq');
            }
        }
    );
    ymaps.layout.storage.add("itex#icon", template);

    initMap();
});

$(document).ready(function () {
    $("#end-route").click(function () {
        if (state == 'edit')
            endLineDrawing();
        else if (state == 'add') {
            cursor.remove();
            $("#end-route").hide();
            map.events.remove('click', addPointArrayHandler);
            state = null;
        }
    });

    $("#cable-filter").change(function() {
        refreshMap();
    });

    $("#addPointOk").click(function () {
        cursor = map.cursors.push('crosshair');
        state = 'add';
        if ($("input[name=point_add_mode]:checked").val() == "single") {
            map.events.add('click', addPointHandler);
        } else {
            map.events.add('click', addPointArrayHandler);
            $("#end-route").show();
        }
        $("#add_point_dialog").modal('hide');
    });

    $("#editNetwork").click(function () {
        setPointsDrag(true);
        toggleMoveButtons();
    });

    $("input[name=point_add_mode]:radio").change(function () {
        $("#point_type_select").toggle(this.value == 'multi');
    });

    $("#saveNetwork").click(function () {
        toggleMoveButtons();
        setPointsDrag(false);
        updateNetwork();
    });

    $("#cancelEdit").click(function () {
        toggleMoveButtons();
        setPointsDrag(false);
        cancelNetwork();
    });

    $("#mapLayer").change(function () {
        refreshMap();
    });

    $("#solve").click(solveRoute);
    $("#clear-solve").click(clearRoute);
    $("#route_from, #route_to").change(highlightPoint);

    $("#addRoute").click(function () {
        cursor = map.cursors.push('crosshair');
        map.events.add('click', startPolylineDrawing);
        state = 'edit';
        $("#end-route").css("display", "block");
    });
});


/// MARK: map

function initMap() {
    map = new ymaps.Map(mapId, {
        center: default_location,
        zoom: user_zoom,
        behaviors: ['default', 'scrollZoom'],
        controls: []
    });
    map.options.set('minZoom', 8);
    map.controls.add('zoomControl', {
        left: '9px',
        top: '40px'
    });
    map.controls.add('fullscreenControl');
    map.controls.add('rulerControl');
    var button = new ymaps.control.Button({
        data: {
            // Зададим иконку для кнопки
            image: 'static/images/route.png',
            // Текст всплывающей подсказки.
            title: 'Нажмите для создания маршрута'
        }
    }, {
        // Зададим опции для кнопки.
        selectOnClick: false
    });
    button.events.add("click", routeButtonClick);
    map.controls.add(button, {top: 5, left: 5});
    map.controls.add('typeSelector', {right: '10px', top: '5px'});

    map.events.add('boundschange', rememberLocation);
    map.events.add('boundschange', resizeIcons);

    $("#mapLayer").val(default_layer);
    refreshMap();
}

function refreshMap(dialog) {
    if (dialog)
        dialog.modal("hide");

    var data = {
            layer: $("#mapLayer").val()
        };
    var cable = $("#cable-filter").val();
    if (cable) {
        data["cable"] = cable
    }

    $.ajax({
        method: "GET",
        url: "/api/routes/",
        data: data
    }).done(function (routes) {
        $("#route_from, #route_to").html("");
        $("#route_from, #route_to").append("<option value='0'>Не выбрано</option>");
        clearRoute();
        map.geoObjects.removeAll();

        ICONS = routes['icons'];

        routes.points.forEach(function (point) {
            createConnectionPoint(point.location, point.id, point.connection, point.type, point.title);
        });

        var allLines = [];
        routes.segments.forEach(function (segment) {
            allLines.push({
                geometry: [segment.start_point, segment.end_point],
                id: segment.id,
                title: segment.cable.title
            });
        });
        var visited = [];
        var lineGroups = [];
        for (var i = allLines.length - 1; i >= 0; i--) {
            if (visited.indexOf(i) > -1) continue;
            var line = allLines[i];
            visited.push(i);
            var conflictingLines = [];
            for (var j = 0; j < i; j++) {
                if (visited.indexOf(j) > -1) continue;
                var aLine = allLines[j];
                if (line.geometry.compare(aLine.geometry) || line.geometry.compare(aLine.geometry.reverse())) {
                    conflictingLines.push(aLine);
                    visited.push(j);
                }
            }
            conflictingLines.push(line);
            lineGroups.push(conflictingLines);
        }
        $.each(lineGroups, function (i, g) {
            (g.length > 1) ? createMultiLine(g) : createLine(g[0].geometry, g[0].id);
        });
    });
}

/// MARK: icons

function currentIconSize() {
    var zoom = map.getZoom();
    var iconSize = 24.0 * (zoom * zoom / max_icon_zoom / max_icon_zoom);
    if (zoom > max_icon_zoom) iconSize = 24;
    return iconSize;
}

function resizeIcons(e) {
    var zoom = e.get('newZoom'), oldZoom = e.get('oldZoom');
    if (zoom != oldZoom) {
        var iconSize = currentIconSize();

        $(".icon_container").css({width: iconSize + 'px', height: iconSize + 'px'});
        $(".icon_container").css({left: -iconSize / 2 + 'px', top: -iconSize / 2 + 'px'});
        $(".icon_container").trigger("shapechange");
    }
}

/// MARK: lines

function endLineDrawing() {
    state = null;
    map.events.remove('click', startPolylineDrawing);
    $("#end-route").css("display", "none");
    cursor.remove();
    if (typeof line === 'undefined') {
        return;
    }

    line.editor.stopEditing();

    if (line.geometry.getCoordinates().length == 0)
        return;

    routeEditBinder();
}

function stateChangeHandler(e) {
    var ev = e.originalEvent;
    if (!ev.newDrawing && ev.oldDrawing)
        endLineDrawing();

}

function startPolylineDrawing(e) {
    if (e.constructor === Array)
    // click on point - e contains coordinates

        line = createLine([e]);
    else
    // click on map - need to get coordinates
        line = createLine([e.get("coords")]);

    map.events.remove('click', startPolylineDrawing);
    line.editor.events.add("statechange", stateChangeHandler);
    line.editor.options.set('drawOver', false);
    line.editor.startEditing();
    line.editor.startDrawing();
}

function lineMouseLeave(e) {
    var tempColor = e.get("target").properties.get("tempColor");
    if (tempColor)
        e.get("target").options.set("strokeColor", tempColor);
    else
        e.get("target").options.set("strokeColor", "FF00FFFF");
}

function createMultiLine(lines) {
    var properties = {
        "tempColor": false
    }, options = {
        draggable: false,
        strokeColor: '#ff00ff',
        strokeWidth: 4,
        zIndex: 10,
        zIndexHover: 100
    }, polyline = new ymaps.Polyline(lines[0].geometry, properties, options);
    polyline.events.add("mouseenter", function (e) {
        e.get("target").options.set("strokeColor", "0000FFFF");
    });
    polyline.events.add("mouseleave", lineMouseLeave);

    var links = [];
    $.each(lines, function (i, line) {
        links.push($("<a href='javascript:void(0)'>" + line.title + "</a>").attr("onclick", "map.balloon.close(); openSegmentDialog('" + line.id + "')").prop('outerHTML'));
    });
    polyline.properties.set("id", guid());
    polyline.properties.set("lines", lines.map(function (line) {
        return line.id;
    }));

    polyline.events.add("click", function (e) {
        var coords = e.get('coords');
        map.balloon.open(coords, {
            contentBody: links.join("<br/>")
        });
    });
    map.geoObjects.add(polyline);
    var shProperties = {}, shOptions = {
        draggable: false,
        strokeColor: '000000FF',
        strokeWidth: 6,
        zIndex: 5,
        zIndexHover: 5
    };
    var shadow = new ymaps.Polyline(lines[1].geometry, shProperties, shOptions);
    map.geoObjects.add(shadow);
}

function createLine(geometry, id) {
    var properties = {}, options = {
        draggable: false,
        strokeColor: 'ff00ffFF',
        strokeWidth: 4,
        zIndex: 10,
        zIndexHover: 100
    }, polyline = new ymaps.Polyline(geometry, properties, options);

    if (id) {
        polyline.properties.set("id", id);

        polyline.events.add("mouseenter", function (e) {
            e.get("target").options.set("strokeColor", "0000FFFF");
        });
        polyline.events.add("mouseleave", lineMouseLeave);
        polyline.events.add("click", function (e) {
            openSegmentDialog(e.get("target").properties.get("id"));
        });
    }

    map.geoObjects.add(polyline);

    return polyline;
}

/// MARK: points

function setPointsDrag(enabled) {
    state = enabled ? "move" : null;
    map.geoObjects.each(function (obj) {
        if (obj.geometry.getType() == "Point") {
            if (enabled) {
                obj.events.add("dragstart", pointDragStart);
                obj.events.add("dragend", pointDragEnd);
                obj.options.set("draggable", true);
            } else {
                obj.events.remove("dragstart", pointDragStart);
                obj.events.remove("dragend", pointDragEnd);
                obj.options.set("draggable", false);
            }
        }
    });
}

function pointClick(e) {
    var coords = e.get("target").geometry.getCoordinates();
    if (line != null)
        line.geometry.insert(line.geometry.getLength(), coords);
    else if (state == 'edit')
        startPolylineDrawing(coords);
    else if (state != 'move') {
        if ($("#solver-block").is(":visible")) {
            var routeSel = ($("#route_from").val() == 0) ? $("#route_from") : $("#route_to");
            routeSel.val(e.get("target").properties.get("id"));
            routeSel.change();
        }
        else
            openPointDialog(e.get("target").properties.get("id"));
    }
}

function pointDragStart(e) {
    var currentImageHref = e.get('target').options.get('iconImageHref');

    var type = e.get("target").properties.get("type");
    var iconHref = ICONS[type]['support-transparent'];
    var id = e.get("target").properties.get("id");


    var connect = e.get("target").properties.get("connection");
    if (connect > 0)
        iconHref = ICONS[type]['connection-transparent'];

    var coords = e.get("target").geometry.getCoordinates();
    var iconSize = currentIconSize();
    var myGeoObject = new ymaps.GeoObject({
        geometry: {
            type: "Point",
            coordinates: coords
        },
        properties: {
            size: iconSize,
            radius: iconSize / 2,
            url: iconHref
        }
    }, {
        iconLayout: "itex#icon"
    });
    if (!movedPoints)
        movedPoints = {};
    if (!movedPoints[id])
        movedPoints[id] = {point: e.get("target"), old: coords};
    map.geoObjects.add(myGeoObject);
    tempPoint = myGeoObject;
}

function pointDragEnd(e) {
    var point = tempPoint.geometry.getCoordinates();
    var after = e.get("target").geometry.getCoordinates();
    updatePointSegments(point, after);
    map.geoObjects.remove(tempPoint);
    tempPoint = null;
}

function updatePointSegments(point, after) {
    map.geoObjects.each(function (obj) {
        if (obj.geometry.getType() != "Point") {
            var coords = obj.geometry.getCoordinates();
            var i = -1;
            if (coords[0].compare(point))
                i = 0;
            else if (coords[1].compare(point))
                i = 1;
            if (i >= 0) {
                coords[i] = after;
                obj.geometry.setCoordinates(coords);
            }
        }
    });
}

function addPointHandler(e) {
    map.events.remove('click', addPointHandler);
    cursor.remove();
    state = null;
    openPointDialog(null, "[" + e.get("coords") + "]");
}

function addPointArrayHandler(e) {
    var id = guid();
    var location = e.get("coords");
    var point = {
        "comment": "",
        "title": "",
        "layer": $("#mapLayer").val(),
        "latitude": location[0],
        "longitude": location[1],
        "type_id": $("#point_type_select").val()
    };

    $.post("/api/point/" + id + "/", {
            point: JSON.stringify(point)
        }
    ).done(function () {
            createConnectionPoint(location, id, false, point.type_id, point.title);
        }).fail(function (err) {
            alert("Ошибка при сохранении: " + err.responseText);
        });

}

function createConnectionPoint(location, id, connection, type, title) {
    if (typeof type === 'undefined' || type == null || !ICONS[type])
        type = 'default';

    var imageHref = ICONS[type]['support'];
    var iconSize = currentIconSize();
    if (typeof connection !== "undefined" && connection && connection != "0")
        imageHref = ICONS[type]['connection'];

    var myGeoObject = new ymaps.GeoObject({
        geometry: {
            type: "Point",
            coordinates: location
        },
        properties: {
            size: iconSize,
            radius: iconSize / 2.0,
            url: imageHref
        }
    }, {
        iconLayout: "itex#icon",
        zIndex: 15
    });
    myGeoObject.properties.set("pType", "connection");
    myGeoObject.properties.set("connection", (connection ? connection : "0"));
    myGeoObject.properties.set("id", (id ? id : guid()));
    myGeoObject.properties.set("type", type);
    myGeoObject.properties.set("defaultIcon", imageHref);
    myGeoObject.events.add("click", pointClick);

    map.geoObjects.add(myGeoObject);

    title = title ? title : "<Не задано>";
    $("#route_from, #route_to").append("<option value='" + id + "'>" + title + "</option>");
}

/// MARK: whole

function updateNetwork() {
    var data = [];
    $.each(movedPoints, function (idx, obj) {
        var id = obj.point.properties.get("id");
        var geometry = obj.point.geometry.getCoordinates();
        data.push({
            id: id,
            latitude: geometry[0],
            longitude: geometry[1]
        });
    });
    $.ajax({
        method: "PATCH",
        url: "/api/point/move/",
        data: JSON.stringify(data)
    }).done(function (response) {
        refreshMap();
    }).fail(function (err) {
        alert(err.responseText);
    });
    movedPoints = null;
}

function cancelNetwork() {
    $.each(movedPoints, function (idx, obj) {
        var after = obj.point.geometry.getCoordinates();
        obj.point.geometry.setCoordinates(obj.old);
        updatePointSegments(after, obj.old);
    });
    movedPoints = null;
}

function toggleMoveButtons() {
    $("#saveNetwork").toggle();
    $("#editNetwork").toggle();
    $("#cancelEdit").toggle();
}

