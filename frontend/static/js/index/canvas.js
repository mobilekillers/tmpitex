/**
 * Created by sergey on 09.12.15.
 */

function Canvas(container) {

    this.segments = [];
    this.markers = [];
    this.markerImages = [];

    this.changedPoints = [];

    this.state = "";

    this.init(container);
}

Canvas.prototype.init = function (container) {
    this.map = new google.maps.Map(container, {
        center: {
            lat: 55.915194,
            lng: 36.871919
        },
        zoom: 15
    });
}

Canvas.prototype.setCenter = function (center) {
    this.map.setCenter({lat: center[0], lng: center[1]});
}

Canvas.prototype.setZoom = function (zoom) {
    this.map.setZoom(zoom);
}

Canvas.prototype.createMarkerImages = function (data) {
    for (var type in data) {
        var icons = [];
        for (var icon_type in data[type]) {

            var icon = new google.maps.MarkerImage(data[type][icon_type],
                null,
                new google.maps.Point(0, 0),
                new google.maps.Point(10, 10),
                new google.maps.Size(20, 20));
            icons[icon_type] = icon;
        }
        this.markerImages[type] = icons;
    }
}

Canvas.prototype.addRoute = function (data) {
}

Canvas.prototype.addSegment = function (data) {
    var startPoint = data["start_point"];
    var endPoint = data["end_point"];
    var polyline = new google.maps.Polyline({
        path: [
            {lat: startPoint[0], lng: startPoint[1]},
            {lat: endPoint[0], lng: endPoint[1]}
        ],
        map: this.map,
        strokeColor: "#ff00ff",
        strokeOpacity: 1,
        strokeWeight: 5,
        segmentId: data['id']
    });

    this.segments.push({
        id: data['id'],
        type: data['cable']['id'],
        line: polyline,
        layer: 'unsetted'
    });

    google.maps.event.addListener(polyline, 'mouseover', function (e) {
        this.setOptions(
            {
                strokeColor: "#0000ff"
            }
        )
    });

    google.maps.event.addListener(polyline, 'mouseout', function () {
        this.setOptions(
            {
                strokeColor: "#ff00ff"
            }
        )
    });

    google.maps.event.addListener(polyline, "click", function (e) {
        openSegmentDialog(this.segmentId);
    });
}

Canvas.prototype.setLayer = function (layer, segmentType) {
    if (typeof(segmentType) === 'undefined') segmentType = '';


    this.markers.forEach(function (val) {
        if (layer == 'all') {
            val['marker'].setOptions({visible: true});
        } else {
            if (val['layer'] == layer) {
                val['marker'].setOptions({visible: true});
            } else {
                val['marker'].setOptions({visible: false});
            }
        }
    });

    var canvas = this;
    this.segments.forEach(function (val) {
        if (val['layer'] == 'unsetted') {
            var matchLayer = [];
            var matches = 0;
            var p1 = val['line'].getPath().getAt(0);
            var p2 = val['line'].getPath().getAt(1);
            for (var j = 0; j < canvas.markers.length; j++) {
                if (canvas.markers[j]['layer'] == layer) {
                    continue;
                }
                if (canvas.markers[j]['marker'].getPosition().lat() == p1.lat()
                    && canvas.markers[j]['marker'].getPosition().lng() == p1.lng()) {
                    matchLayer.push(canvas.markers[j]['layer']);
                    matches += 1;
                }
                else if (canvas.markers[j]['marker'].getPosition().lat() == p2.lat()
                    && canvas.markers[j]['marker'].getPosition().lng() == p2.lng()) {
                    matchLayer.push(canvas.markers[j]['layer']);
                    matches += 1;
                }
                if (matches == 2) {
                    if (matchLayer[0] == matchLayer[1]) {
                        val['layer'] = matchLayer[0];
                    } else {
                        val['layer'] = 'all';
                    }
                    break;
                }
            }
        }
        if (layer == 'all') {
            if (segmentType == '') {
                val['line'].setOptions({visible: true});
            } else {
                if (val['type'] == segmentType) {
                    val['line'].setOptions({visible: true});
                } else {
                    val['line'].setOptions({visible: false});
                }
            }
        } else {
            if (val['layer'] == layer) {
                if (segmentType == '') {
                    val['line'].setOptions({visible: true});
                } else {
                    if (val['type'] == segmentType) {
                        val['line'].setOptions({visible: true});
                    } else {
                        val['line'].setOptions({visible: false});
                    }
                }
            } else {
                val['line'].setOptions({visible: false});
            }
        }
    });
}

Canvas.prototype.segmentFilter = function (segmentType) {
    this.segments.forEach(function (val) {
        if (segmentType == '') {
            val['line'].setOptions({visible: true});
        } else {
            if (val['type'] == segmentType) {
                val['line'].setOptions({visible: true});
            } else {
                val['line'].setOptions({visible: false});
            }
        }
    });
}

Canvas.prototype.removeMarker = function (marker, withSegment) {
    if (withSegment) {
        var segmentsToRemove = [];
        for (var j = 0; j < this.segments.length; j++) {
            var a = this.segments[j]['line'].getPath();
            var b = marker['marker'].getPosition();

            if (Math.abs(a.getAt(0).lat() - b.lat()) < 0.0000001 &&
                (Math.abs(a.getAt(0).lng() - b.lng())) < 0.0000001) {
                segmentsToRemove.push(this.segments[j]);
            }
            else if (Math.abs(a.getAt(1).lat() - b.lat()) < 0.0000001 &&
                (Math.abs(a.getAt(1).lng() - b.lng())) < 0.0000001) {
                segmentsToRemove.push(this.segments[j]);
            }
        }

        for(j = 0; j < segmentsToRemove.length; j++) {
            this.removeSegment(segmentsToRemove[j]);
        }
    }

    marker['marker'].setMap(null);

    var index = this.markers.indexOf(marker);
    if (index > -1) {
        this.markers.splice(index, 1);
    }
}

Canvas.prototype.hasMarkerId = function (markerId) {
    var res = false;
    this.markers.forEach(function (val) {
        if (val['id'] == markerId) {
            res = true;
            //break;
        }
    });
    return res;
}


Canvas.prototype.removeSegment = function (segment) {
    segment['line'].setMap(null);
    var index = this.segments.indexOf(segment);
    if (index > -1) {
        this.segments.splice(index, 1);
    }
}

Canvas.prototype.addMarker = function (data) {
    var type = data['type'];
    var canvas = this;

    if (typeof type === 'undefined' || type == null || !this.markerImages[type])
        type = 'default';

    var markerImage = this.markerImages[type]['support'];

    if (typeof data['connection'] !== 'undefined' && data['connection'] && data['connection'] != "0") {
        markerImage = this.markerImages[type]['connection'];
    }

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(data["location"][0], data["location"][1]),
        map: canvas.map,
        icon: markerImage,
        title: data["title"],
        markerId: data['id']
    });
    var pType = "";
    if ("connection" in data) {
        pType = "connection";
    }

    this.markers.push({
        id: data['id'],
        layer: data['layer'],
        marker: marker,
        pType: pType
    });

    google.maps.event.addListener(marker, 'click', function (e) {
        for (var i = 0; i < canvas.markers.length; i++) {
            if (canvas.markers[i]['marker'].getPosition().lat() == e.latLng.lat()
                && canvas.markers[i]['marker'].getPosition().lng() == e.latLng.lng()) {
                if (canvas.state != "edit") {
                    openPointDialog(canvas.markers[i]['id']);
                }
                else {
                    if (routePoly != null) {
                        var path = routePoly.getPath();
                        path.push(e.latLng);
                    }
                }
            }
        }
    });
}

Canvas.prototype.addMarkersClick = function () {
    for (var i = 0; i < this.markers.length; i++) {
        google.maps.event.addListener(this.markers[i]['marker'], 'click', function (e) {
            for (var i = 0; i < canvas.markers.length; i++) {
                if (canvas.markers[i]['marker'].getPosition().lat() == e.latLng.lat()
                    && canvas.markers[i]['marker'].getPosition().lng() == e.latLng.lng()) {
                    if (canvas.state != "edit") {
                        openPointDialog(canvas.markers[i]['id']);
                    }
                    else {
                        if (routePoly != null) {
                            var path = routePoly.getPath();
                            path.push(e.latLng);
                        }
                    }
                }
            }
        });
    }
}

Canvas.prototype.cleanMarkersClick = function () {
    for (var i = 0; i < this.markers.length; i++) {
        google.maps.event.clearListeners(this.markers[i]['marker'], 'click');
    }
}

Canvas.prototype.editMode = function () {
    this.cleanMarkersClick();
    this.changedPoints = [];
    var canvas = this;
    var point_segments = [];
    for (var i = 0; i < this.markers.length; i++) {
        google.maps.event.addListener(this.markers[i]['marker'], 'mouseover', function (e) {
            this.setDraggable(true);
            point_segments = [];
            var coords = this.getPosition();
            for (var j = 0; j < canvas.segments.length; j++) {
                var a = canvas.segments[j]['line'].getPath();
                var b = coords;
                if (Math.abs(a.getAt(0).lat() - b.lat()) < 0.0000001 &&
                    (Math.abs(a.getAt(0).lng() - b.lng())) < 0.0000001) {
                    point_segments.push({
                        segment: canvas.segments[j]['line'],
                        start: true,
                        end: false
                    });
                }
                else if (Math.abs(a.getAt(1).lat() - b.lat()) < 0.0000001 &&
                    (Math.abs(a.getAt(1).lng() - b.lng())) < 0.0000001) {
                    point_segments.push({
                        segment: canvas.segments[j]['line'],
                        start: false,
                        end: true
                    });
                }
            }
        });
        //dragstart
        google.maps.event.addListener(this.markers[i]['marker'], 'dragstart', function (e) {
            this.setOpacity(0.5);
            for (var j = 0; j < point_segments.length; j++) {
                point_segments[j]['segment'].setOptions(
                    {
                        strokeOpacity: 0.5
                    }
                );
            }
        });
        google.maps.event.addListener(this.markers[i]['marker'], 'dragend', function (e) {
            this.setOpacity(1);
            for (var j = 0; j < point_segments.length; j++) {
                point_segments[j]['segment'].setOptions(
                    {
                        strokeOpacity: 1
                    }
                );
            }
            canvas.changedPoints.push(this);
        });
        google.maps.event.addListener(this.markers[i]['marker'], 'drag', function (e) {
            var pos = this.getPosition();
            for (var j = 0; j < point_segments.length; j++) {
                if (point_segments[j]['start']) {
                    point_segments[j]['segment'].setPath([pos, point_segments[j]['segment'].getPath().getAt(1)]);
                }
                else if (point_segments[j]['end']) {
                    point_segments[j]['segment'].setPath([point_segments[j]['segment'].getPath().getAt(0), pos]);
                }
            }
        });
    }
}

Canvas.prototype.doneEditMode = function () {
    var changed = this.changedPoints;
    this.changedPoints = [];
    for (var i = 0; i < this.markers.length; i++) {
        google.maps.event.clearListeners(this.markers[i]['marker'], 'mouseover');
        google.maps.event.clearListeners(this.markers[i]['marker'], 'dragstart');
        google.maps.event.clearListeners(this.markers[i]['marker'], 'dragend');
        google.maps.event.clearListeners(this.markers[i]['marker'], 'drag');
        this.markers[i]['marker'].setDraggable(false);

        for (var j = 0; j < changed.length; j++) {
            if (this.markers[i]['marker'] == changed[j]) {
                this.changedPoints.push(this.markers[i]);
            }
        }

    }
    this.addMarkersClick();
}

Canvas.prototype.cancelEditMode = function(getPointCallback, getSegmentCallback) {
    this.doneEditMode();

    for(var i = 0; i < this.changedPoints.length; i++) {
        var id = this.changedPoints[i]['id'];

        var coords = this.changedPoints[i]['marker'].getPosition();
        for (var j = 0; j < this.segments.length; j++) {
            var a = this.segments[j]['line'].getPath();
            var b = coords;

            if (Math.abs(a.getAt(0).lat() - b.lat()) < 0.00000001 &&
                (Math.abs(a.getAt(0).lng() - b.lng())) < 0.0000001) {
                getSegmentCallback(this.segments[j]['id']);
            }
            else if (Math.abs(a.getAt(1).lat() - b.lat()) < 0.0000001 &&
                (Math.abs(a.getAt(1).lng() - b.lng())) < 0.0000001) {
                getSegmentCallback(this.segments[j]['id']);
            }
        }

        this.removeMarker(this.changedPoints[i], true);

        getPointCallback(id);
    }
}

Canvas.prototype.addPointsMode = function (addPointHandler) {
    canvas.map.setOptions({draggableCursor: 'crosshair'});
    google.maps.event.addListener(this.map, 'click', addPointHandler);
}

Canvas.prototype.doneAddPointsMode = function () {
    canvas.map.setOptions({draggableCursor: null});
    google.maps.event.clearListeners(this.map, 'click');
}

Canvas.prototype.setOpacity = function (opacity) {
    this.markers.forEach(function (val) {
        val['marker'].setOptions({opacity: opacity});
    });
}