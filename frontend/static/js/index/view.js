/**
 * Created by sergey on 09.12.15.
 */

var canvas = new Canvas(document.getElementById("map"));

var editActive = false;

var points = [];
var segments = [];

var data = {
    layer: 'all'
};

function getData(routes, canvas) {
    canvas.createMarkerImages(routes['icons']);
    points = routes['points'];
    segments = routes['segments'];
}

$.ajax({
    method: "GET",
    url: "/api/routes/",
    data: data,
    success: function (routes) {
        getData(routes, canvas);
    }
});

var loadedPoints = false;
var loadedSegments = false;

var oldMarkersLen = 0;
var oldSegmentsLen = 0;

var k = 2;
var count = 1;

function drawPoint(point) {
    for (var i = 0; i < canvas.markers.length; i++) {
        if (canvas.markers[i]['id'] == point['id']) {
            canvas.removeMarker(canvas.markers[i], false);
        }
    }

    canvas.addMarker(point);
    canvas.setLayer($("#mapLayer").val());
}

function removeSegment(data) {
    for (var i = 0; i < canvas.segments.length; i++) {
        if (canvas.segments[i]['id'] == data) {
            canvas.removeSegment(canvas.segments[i]);
        }
    }

    $("#segment_container").modal('hide');
}

function addNewSegment(data) {
    for (var i = 0; i < data['points'].length; i++) {
        if(!canvas.hasMarkerId(data['points'][i]['id'])) {
            canvas.addMarker(data['points'][i]);
        }
    }
    for (var i = 0; i < data['segments'].length; i++) {
        canvas.addSegment(data['segments'][i]);
    }
}

function removePoint(point) {
    for (var i = 0; i < canvas.markers.length; i++) {
        if (canvas.markers[i]['id'] == point['id']) {
            canvas.removeMarker(canvas.markers[i], true);
            break;
        }
    }

    $("#point_container").modal('hide');
}

function drawPoints() {
    if (points.length > 0) {
        count += 1;
        var elements = points.splice(0, Math.pow(count, k));
        for (var i = 0; i < elements.length; i++) {
            canvas.addMarker(elements[i]);
        }
    }
    else if (canvas.markers.length > oldMarkersLen) {
        loadedPoints = true;
    }

    if (!loadedPoints)
        setTimeout(drawPoints, 0);
    else {
    }
}

function drawSegments() {
    if (segments.length > 0) {
        count += 1;
        var elements = segments.splice(0, Math.pow(count, k));
        for (var i = 0; i < elements.length; i++) {
            canvas.addSegment(elements[i]);
        }
    }
    else if (canvas.segments.length > oldSegmentsLen) {
        loadedSegments = true;
    }

    if (!loadedSegments)
        setTimeout(drawSegments, 0);
}

function updateNetwork() {
    var data = [];
    for (var i = 0; i < canvas.changedPoints.length; i++) {
        data.push({
            id: canvas.changedPoints[i]['id'],
            latitude: canvas.changedPoints[i]['marker'].getPosition().lat(),
            longitude: canvas.changedPoints[i]['marker'].getPosition().lng()
        })
    }
    $.ajax({
        method: "PATCH",
        url: "/api/point/move/",
        data: JSON.stringify(data)
    }).fail(function (err) {
        alert(err.responseText);
    });
}

prepareSelection();

drawPoints();
drawSegments();

$(document).ready(function () {
    var body = $("body");


    body.bind("pointSaved", function () {
        if ($("#point_container"))
            $("#point_container").modal("hide");
    });

    $("#editNetwork").click(function () {
        if(editActive) {
            return;
        }
        editActive = true;
        canvas.editMode();
        toggleMoveButtons();
    });
    $("#cancelEdit").click(function () {
        editActive = false;
        toggleMoveButtons();
        oldMarkersLen = canvas.markers.length;
        oldSegmentsLen = canvas.segments.length;
        canvas.cancelEditMode(getPointCallback, getSegmentCallback);
        loadedSegments = false;
        loadedPoints = false;
        drawPoints();
        drawSegments();
    });
    $("#saveNetwork").click(function () {
        editActive = false;
        toggleMoveButtons();
        canvas.doneEditMode();
        updateNetwork();
    });
    $("#addPointOk").click(function () {
        if ($("input[name=point_add_mode]:checked").val() == "single") {
            canvas.addPointsMode(addPointHandler);
        } else {
            canvas.addPointsMode(addPointsHandler);
            $("#end-route").show();
        }
        $("#add_point_dialog").modal('hide');
    });
    $("#end-route").click(function () {
        editActive = false;
        $("#end-route").hide();
        canvas.doneAddPointsMode();
    });

    $("#cable-filter").change(function () {
        var cable = $("#cable-filter").val();
        var layer = $("#mapLayer").val();
        canvas.setLayer(layer, cable);
    });
    $("#mapLayer").change(function () {
        var layer = $("#mapLayer").val();
        var cable = $("#cable-filter").val();
        canvas.setLayer(layer, cable);
    });

    $("#addRoute").click(function () {
        if(editActive) {
            return;
        }
        editActive = true;

        canvas.map.setOptions({draggableCursor: 'crosshair'});
        routePoly = new google.maps.Polyline({
            strokeColor: "#ff00ff",
            strokeOpacity: 1,
            strokeWeight: 5,
            editable: true

        });
        routePoly.setMap(canvas.map);

        addTmpRouteListener = canvas.map.addListener('click', addRoutePoint);

        finishInfo = new google.maps.InfoWindow({});
        google.maps.event.addListener(routePoly, "rightclick", function (e) {
            var points = this.getPath();
            if(points.length > 1) {
                var lastpoint = points.getAt(points.length - 1);
                finishInfo.setContent('<button onclick="finishClicked()">Завершить</button>' +
                                       '<button onclick="clearState()">Отмена</button>');
                finishInfo.setPosition(lastpoint);
                finishInfo.open(canvas.map);
            }
        });
        canvas.state = 'edit';
        $("#end-route").css("display", "block");
    });
    $("#end-route").click(function () {
        finishClicked();
    });

    $("input[name=point_add_mode]:radio").change(function () {
        $("#point_type_select").toggle(this.value == 'multi');
    });
});

function finishClicked() {
   if(finishInfo) {
        finishInfo.setMap(null);
   }
   editActive = false;
   if (canvas.state == 'edit')
       endRouteDrawing();
   else if (canvas.state == 'add') {
       cursor.remove();
       $("#end-route").hide();
       map.events.remove('click', addPointArrayHandler);
       canvas.state = null;
   }
}


window.onbeforeunload = function(e) {
    var location = canvas.map.getCenter();
    $.ajax({
         url: "/api/settings/",
         method: "PUT",
         async: false,
         data: {
             'latitude': location.lat(),
             'longitude': location.lng(),
             'zoom': canvas.map.getZoom(),
             'layer': $("#mapLayer").val()
         }
    }).done(function (response) {

    }).fail(function (err) {
         console.log(err.responseText);
    });

};


function getPointCallback(pointId) {
    $.get("/api/point/" + pointId + "/").done(function (data) {
        points.push(data);
    }).fail(function (err) {
        alert("Ошибка при получении точки: " + err.responseText);
    });
}

function getSegmentCallback(segmentId) {
    $.get("/api/segment/" + segmentId + "/").done(function (data) {
        segments.push(data);
    }).fail(function (err) {
        alert("Ошибка при получении сегмента: " + err.responseText);
    });
}

function addPointHandler(e) {
    canvas.doneAddPointsMode();
    openPointDialog(null, "[" + e.latLng.lat() + "," + e.latLng.lng() + "]");
}

function addPointsHandler(e) {
    var id = guid();
    var point = {
        "comment": "",
        "title": "",
        "layer": $("#mapLayer").val(),
        "latitude": e.latLng.lat(),
        "longitude": e.latLng.lng(),
        "type_id": $("#point_type_select").val()
    };

    $.post("/api/point/" + id + "/", {
            point: JSON.stringify(point)
        }
    ).done(function (data) {
        drawPoint(data)
    }).fail(function (err) {
        alert("Ошибка при сохранении: " + err.responseText);
    });
}

function toggleMoveButtons() {
    $("#saveNetwork").toggle();
    $("#editNetwork").toggle();
    $("#cancelEdit").toggle();
}

function addRoutePoint(event) {
    var path = routePoly.getPath();
    path.push(event.latLng);
}


function endRouteDrawing() {
    canvas.state = null;
    google.maps.event.removeListener(addTmpRouteListener);
    canvas.map.setOptions({draggableCursor: ''});
    $("#end-route").css("display", "none");

    if (typeof routePoly === 'undefined') {
        return;
    }

    if (routePoly.getPath().getLength() == 0)
        return;

    routeEditBinder();
}