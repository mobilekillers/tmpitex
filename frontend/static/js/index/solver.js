/**
 * Created by cloud on 21.09.15.
 */
$(document).ready(function () {
    $("img.img-switch").click(function () {
        var t = $("#route_from").val();
        $("#route_from").val($("#route_to").val());
        $("#route_to").val(t);
    });
});

function rememberLocation() {
    var location = map.getCenter();
    $.ajax({
        url: "/api/settings/",
        method: "PUT",
        async: false,
        data: {
            'latitude': location.lat(),
            'longitude': location.lng(),
            'zoom': map.getZoom(),
            'layer': $("#mapLayer").val()
        }
    }).done(function (response) {

    }).fail(function (err) {
        console.log(err.responseText);
    });
}

function highlightPoint() {
    var id = $(this).val();
    var is_from = $(this).is("#route_from");
    var prevHighlight = is_from ? route_from : route_to;
    if (prevHighlight) {
        var iconHref = prevHighlight.properties.get("defaultIcon");
        prevHighlight.options.set("iconImageHref", iconHref);
    }
    if (id == 0 && is_from) route_from = null;
    if (id == 0 && !is_from) route_to = null;
    map.geoObjects.each(function (obj) {
        if (obj.properties.get("id") == id) {
            var type = obj.properties.get("type");
            var imgHref = ICONS[type]['selected'];
            obj.properties.set("url", imgHref);
            obj.options.set("zIndex", 30);
            is_from ? route_from = obj : route_to = obj;
            return false;
        }
    });
}

function solveRoute() {
    var from = $("#route_from").val();
    var to = $("#route_to").val();
    clearRouteHighlight();
    $("#route-info").hide();
    if (!from || !to) {
        alert("Выберите точки");
        return;
    }
    $.ajax({
        method: "POST",
        url: "/solve/solve.php",
        data: {
            "pid": from,
            "target": to
        }
    }).done(function (response) {
        var data = JSON.parse(response);

        if (data.routes.length == 0) {
            alert("Маршрут не найден!");
            return;
        }
        $("#route-info").html(data.markup);
        $("#route-info").show();
        $("#route-info input[name=route]").change(function () {
            clearRouteHighlight();
            var path = data.routes[$(this).val()];
            var ids = [];
            for (var i = 0; i < path.route.length; i++)
                if (path.route[i]["type"] == "segment" || path.route[i]["type"] == "point")
                    ids.push(path.route[i]["id"]);

            highlightedObjects = [];
            map.geoObjects.each(function (obj) {
                // проверка остальных объектов - точек и
                var idInArray = $.inArray(obj.properties.get("id"), ids) > -1;
                var lines = obj.properties.get("lines");
                if (lines) {
                    var intersectedIds = lines.filter(function (n) {
                        return ids.indexOf(n) != -1
                    });
                    idInArray = idInArray || intersectedIds.length > 0;
                }
                if (!idInArray) return;

                if (obj.geometry.getType() == "Point") {
                    var type = obj.properties.get("type");
                    var imgHref = ICONS[type]["selected"];
                    obj.properties.set("url", imgHref);
                }
                else {
                    obj.properties.set("tempColor", "ede22fFF");
                    obj.options.set("strokeColor", "ede22fFF");
                }
                highlightedObjects.push(obj);
            });
        });

        $("#route-info .route-details").click(function () {
            var path = data.routes[$(this).data("id")];
            var infoDiv = $("#route-details .route-path");
            infoDiv.html("");
            var routeList = $("<ol class='route-list'></ol>");
            var plotData = [];
            var currentPlotData = [0, 0];
            for (var i = 0; i < path.route.length; i++) {
                var hop = path.route[i];
                var li = $("<li></li>");
                if (hop.type == "point") {
                    li.append("<b>Точка: " + hop.title + "</b><br/>")
                        .append("Координаты: " + hop.location + "<br/>");
                    if (hop.welding > 0) {
                        li.append("Затухание сварки: " + hop.welding.toFixed(2) + " дб<br/>");
                        plotData.push(currentPlotData);
                        currentPlotData = [currentPlotData[0], currentPlotData[1] + hop.welding];
                    }
                }
                else if (hop.type == "segment") {
                    li.append("<b>Сегмент: " + hop.data.title + "</b><br/>")
                        .append("Длина: " + (hop.data.length * 1000).toFixed(2) + " м<br/>")
                        .append("Затухание: " + hop.data.attenuation.toFixed(2) + " дб<br/>");
                    plotData.push(currentPlotData);
                    currentPlotData = [currentPlotData[0] + hop.data.length * 1000, currentPlotData[1] + hop.data.attenuation];
                }
                else {
                    li.append("<b>Сплиттер</b><br/>")
                        .append((hop.data.direct ? "Вход " : "Выход ") + hop.data.input + " -> " +
                        (hop.data.direct ? "Выход " : "Вход ") + hop.data.output + "<br/>")
                        .append("Затухание: " + hop.data.attenuation + " дб<br/>");
                    plotData.push(currentPlotData);
                    currentPlotData = [currentPlotData[0], currentPlotData[1] + hop.data.attenuation];
                }
                routeList.append(li);
            }
            plotData.push(currentPlotData);
            $.plot($(".route-plot"), [plotData], {
                    grid: {
                        margin: {
                            left: 40
                        },
                        labelMargin: 10
                    },
                    axisLabels: {
                        show: true
                    },
                    xaxes: [{
                        axisLabel: 'Расстояние, м',
                        axisLabelPadding: 12
                    }],
                    yaxes: [{
                        position: 'left',
                        axisLabel: 'Затухание, Дб'
                    }]
                }
            );
            infoDiv.append(routeList);
            $("#route-details").modal();
        });
        $(".route-toggle-all").click(function () {
            $(".additional").toggle();
        });
        $("#route-info input:checked").change();
    });
}

function clearRouteHighlight() {
    if (!highlightedObjects) return;
    var from_id = route_from ? route_from.properties.get("id") : null;
    var to_id = route_to ? route_to.properties.get("id") : null;
    for (var i = 0; i < highlightedObjects.length; i++) {
        var obj = highlightedObjects[i]
        var id = obj.properties.get("id");

        if (obj.geometry.getType() == "Point" && id != from_id && id != to_id) {
            obj.properties.set("url", obj.properties.get("defaultIcon"));
        }
        else {
            obj.options.set("strokeColor", "FF00FFFF");
            obj.properties.set("tempColor", null);
        }
    }
    highlightedObjects = null;
}

function clearRoute() {
    $("#route-info").hide();
    $("#route_from, #route_to").val(0);
    $("#route_from, #route_to").change();
    clearRouteHighlight();
}

function routeButtonClick() {
    $("#solver-block").toggle();
    if (!$("#solver-block").is(":visible"))
        clearRoute();
}
