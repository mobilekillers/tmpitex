/**
 * Created by cloud on 21.09.15.
 */
function routeEditBinder() {
    var points = routePoly.getPath();

    var existingPoints = [];
    var len = 0;
    for (var i = 0; i < points.length - 1; i++)
        len = google.maps.geometry.spherical.computeDistanceBetween(points.getAt(i), points.getAt(i + 1)) / 1000.0;

    route = []
    for (var i = 0; i < points.getLength(); i++) {
        var p = null;
        canvas.markers.forEach(function (obj) {
//if (obj.properties.get("pType") == "connection" && obj.geometry.getCoordinates() == points[i])
            if(obj['pType'] == "connection"){
                if (obj['marker'].getPosition().lat() == points.getAt(i).lat()
                && obj['marker'].getPosition().lng() == points.getAt(i).lng()) {
                        p = existingPoints[i] = obj;
                }
            }
        });
        if (p) {
            var coords = p['marker'].getPosition();
            route.push({
                id: p.id,
                latitude: coords.lat(),
                longitude: coords.lng()
            });
        }
        else {
            route.push({
                id: guid(),
                latitude: points.getAt(i).lat(),
                longitude: points.getAt(i).lng()
            });
        }
    }
    $("#route-length").text(len.toFixed(3));
    $("#route-edit-form").modal();
    $("#route-points").text(points.length);
}

function initRouteDialog() {
    $("#save_route").click(
        function () {
            saveRoute();
            $("#route-edit-form").modal("hide");
        });
    $("#cancel_route").click(function () {
        //map.geoObjects.remove(line);
        clearState();
        $("#route-edit-form").modal("hide");
    });
}

function saveRoute() {
    if(route == null) {
        clearState();
        return;
    }

    var routeCable = $("#route-cable").val();
    var routeLayer = $("#mapLayer").val();

    $.post("/api/routes/", {
        points: JSON.stringify(route),
        cable: routeCable,
        layer: routeLayer
    }).done(function (data) {
        parent.$("body").trigger("segmentAdded",[data]);
        prevRoute = null;
        //refreshMap();
    }).fail(function () {
        alert("Ошибка при сохранении");
        prevRoute = null
        //refreshMap();
    });
    clearState();
}

function clearState() {
    $("#end-route").hide();
    if(finishInfo) {
        finishInfo.setMap(null);
    }
    route = null;
    line = null;
    canvas.state = '';
    editActive = false;
    routePoly.setMap(null);
}

function openPointDialog(id, location) {
    var src = "/point" + (id ? ("?id=" + id) : "");
    if (location)
        src = src + (id ? "&" : "?") + "location=" + location;
    src += ((src.indexOf("?") == -1) ? "?" : "&") + "layer=" + $("#mapLayer").val();
    $("body").loading();
    $('#point_container iframe').attr("src", src).load(function () {
        $("#point_container").modal();
        $("body").loading(false);
    });
}

function openSegmentDialog(id) {
    var src = "/segment/" + id + "/";
    $("body").loading();
    $('#segment_container iframe').attr("src", src).load(function () {
        $("#segment_container").modal();
        $("body").loading(false);
    });
}

function openConnection(id, cid, create, canSaveConnection) {
    var isCommonSchema = (cid == null && create == false && canSaveConnection == false);
    var src = "/connection?id=" + id + (cid ? "&cid=" + cid : "") + (create ? "&isNew=1" : "") + (canSaveConnection ? "&canSaveConnection=1" : "") + (isCommonSchema ? "&isCommonSchema=1" : "");

    var connectionContainer = $("#connection_container");
    var frm = $('#connection_container iframe');
    frm.attr("src", src).load(function () {
        connectionContainer.modal();
    });
}

function newCableDialog() {
    var src = "cable_form.php";
    var cableFrame = $('#cable iframe');
    cableFrame.css("display", "none");
    cableFrame.attr("src", src);
    cableFrame.load(function () {
        cableFrame.css("display", "block");
    });
    $("#cable").modal();
}

function cableSaved() {
    $("#cable").modal("hide");
    alert("Кабель сохранен");
    bindCables();
}

function bindCables() {
    $.ajax({
        method: "GET",
        url: "/api/cables/"
    }).done(function (cables) {
        $("#route-cable").html("");
        cables.forEach(function (cable) {
            $("#route-cable").append($('<option></option>').val(cable.id).html(cable.title));
        });
    });
}