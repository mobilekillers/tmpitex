/**
 * Created by cloud on 21.09.15.
 */

var circle = {
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: 'red',
    fillOpacity: .4,
    scale: 4.5,
    strokeColor: 'white',
    strokeWeight: 1
};

var tmpMarkers = new Array();
var selectionPolygon = null;
var addTmpMarkerListner = null;

function prepareSelection() {
    $("#btnSelect").on("click", function () {
        if(editActive) {
            return;
        }
        editActive = true;
        toggleSelectionButtons();

        canvas.setOpacity(0.45);

        poly = new google.maps.Polyline({
            strokeColor: '#000000',
            strokeOpacity: 1.0,
            zIndex: 2000,
            strokeWeight: 2
        });
        poly.setMap(canvas.map);

        canvas.map.setOptions({draggableCursor: 'crosshair', disableDoubleClickZoom: true});
        addTmpMarkerListner = canvas.map.addListener('click', addLatLng);

        // Handles click events on a map, and adds a new point to the Polyline.

    });

    $("#btnSelectCancel").on("click", finishPointsMigration);
}

$(document).ready(function () {
    $("#migrate-points").click(function () {
        var layer = "all";
        if (!$("#planCheck").prop("checked")) {
            layer = "fact";
        }
        else if (!$("#factCheck").prop("checked")) {
            layer = "plan";
        }
        var points = [];

        canvas.markers.forEach(function (val) {
            if (google.maps.geometry.poly.containsLocation(val['marker'].position, selectionPolygon)) {
                if (val['marker'].visible) {
                    val['marker'].visible
                    val['layer'] = layer;
                    points.push(val['id']);
                }
            }
        });


        canvas.segments.forEach(function(val) {
            val['layer'] = 'unsetted';
                // if(google.maps.geometry.poly.containsLocation(val['marker'].position, selectionPolygon)) {
                // if(val['marker'].visible) {
                // val['marker'].visible
                // val['layer'] = layer;
                // points.push(val['id']);
                // }
                // }
        });

        var data = {
            points: points,
            layer: layer
        };

        $.ajax({
            method: "PATCH",
            url: "/api/point/layer/",
            data: JSON.stringify(data)
        }).done(function (response) {
            $("#migrate-points-form").modal("hide");
            finishPointsMigration();
            canvas.setLayer($("#mapLayer").val());

        }).fail(function (err) {
            alert(err.responseText);
        });
    });
});

function finishPointsMigration() {
    editActive = false;
    canvas.setOpacity(1.0);
    canvas.map.setOptions({draggableCursor: '', disableDoubleClickZoom: false});
    toggleSelectionButtons();
    //if(addTmpMarkerListner != null) {
    google.maps.event.removeListener(addTmpMarkerListner);
    //addTmpMarkerListner = null;
    //}
    if (selectionPolygon != null) {
        selectionPolygon.setMap(null);
    } else {
        poly.setMap(null);
        tmpMarkers.forEach(function (val, ind) {
            val.setMap(null);
        });
        tmpMarkers = [];
    }
    $("#migrate-points-form").modal("hide");
}


function createSelectionPolygon(event) {
    var path = poly.getPath();
    if (path.getLength() > 2) {
        selectionPolygon = new google.maps.Polygon({
            map: canvas.map,
            paths: path.getArray(),
            strokeColor: '#000000',
            strokeWeight: 2,
            fillColor: '#FF0000',
            editable: true,
            fillOpacity: 0.45,
            //draggable: true,
            zIndex: 1,
            geodesic: true
        });
        poly.setMap(null);
        tmpMarkers.forEach(function (val, ind) {
            val.setMap(null);
        });
        tmpMarkers = [];
        selectionPolygon.addListener('dblclick', function (e) {
            showMovePointsDialog();
        });
        selectionPolygon.addListener('rightclick', function (e) {
            showMovePointsDialog();
        });

        google.maps.event.removeListener(addTmpMarkerListner);
        highlightMarkersInSelected();

        selectionPolygon.getPath().addListener('set_at', highlightMarkersInSelected);
        selectionPolygon.getPath().addListener('insert_at', highlightMarkersInSelected);

    }
}

function highlightMarkersInSelected() {
    canvas.markers.forEach(function (val) {
        if (google.maps.geometry.poly.containsLocation(val['marker'].position, selectionPolygon)) {
            val['marker'].setOptions({opacity: 0.85});
        } else {
            val['marker'].setOptions({opacity: 0.4});
        }
    });
}

function addLatLng(event) {
    var path = poly.getPath();
    path.push(event.latLng);
    var marker = new google.maps.Marker({
        icon: circle,
        position: event.latLng,
        title: '#' + path.getLength(),
        zIndex: 1000,
        map: canvas.map
    });
    if (path.getLength() == 1) {
        marker.addListener('click', createSelectionPolygon);
    }
    tmpMarkers.push(marker);
}


function showMovePointsDialog() {
    var factChecked = !($("#mapLayer").val() == 'plan');
    var planChecked = !($("#mapLayer").val() == 'fact');
    $("#factCheck").prop("checked", factChecked);
    $("#planCheck").prop("checked", planChecked);
    $("#migrate-points-form").modal();
}

function toggleSelectionButtons() {
    $("#btnSelect").toggle();
    $("#btnSelectCancel").toggle();
}


